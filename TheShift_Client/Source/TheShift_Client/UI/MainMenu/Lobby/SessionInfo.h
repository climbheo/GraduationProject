﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Misc/UMapTypes.h"

#include "SessionInfo.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THESHIFT_CLIENT_API USessionInfo : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USessionInfo();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadOnly)
	int SessionIndex;
	UPROPERTY(BlueprintReadOnly)
	FString SessionName;
	UPROPERTY(BlueprintReadOnly)
	TArray<int> ClientIds;
	UPROPERTY(BlueprintReadOnly)
	bool IsInGame;
	UPROPERTY(BlueprintReadOnly)
	UMapType Map;
		
};
