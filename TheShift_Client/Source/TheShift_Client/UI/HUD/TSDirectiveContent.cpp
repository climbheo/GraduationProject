﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSDirectiveContent.h"

#include "TheShift_ClientGameModeBase.h"
#include "Actors/TSTriggerBox.h"
#include "Protocol/Log.h"
#include "Protocol/Serializables.h"
#include "TheShift_GameInstanceBase.h"
#include "Network/Components/TSNetworkedComponent.h"
#include "Item/TSChest.h"
#include "UserWidget.h"
#include "UI/Items/TSChestInventoryWidget.h"

// Sets default values for this component's properties
UTSDirectiveContent::UTSDirectiveContent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	static ConstructorHelpers::FClassFinder<UUserWidget> DefaultChestInventoryWidget(TEXT("/Game/Graphic/UI/Items/ChestInventory.ChestInventory_C"));
	if(DefaultChestInventoryWidget.Succeeded())
	{
		ChestInventoryWidgetClass = DefaultChestInventoryWidget.Class;
	}
}


// Called when the game starts
void UTSDirectiveContent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTSDirectiveContent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

// Directive에 의한 widget생성
UUserWidget* UTSDirectiveContent::CreateWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	auto ValidWidget = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->ChangeIngameWidget(NewWidgetClass);

	// Widget이 유효하고 부모 trigger에 종속된 widget이라면 trigger에 추가시켜준다. (
	if (ValidWidget && ParentTrigger)
	{
		ParentTrigger->CurrentActiveWidgets.Emplace(ValidWidget);
	}

	return ValidWidget;
}

void UTSDirectiveContent::ApplyContent()
{
	switch(DirectiveType)
	{
	case EInteractableActionType::OpenDoorRotatePlus:
	{
		if (Targets.Num() != 0)
		{
			LOG_INFO("Open door plus");
			TheShift::Serializables::InteractableAct Data;
			// UID를 얻어와서 Data.Value에 넣어준다.
			if (auto NetworkComp = Cast<UTSNetworkedComponent>(Targets[0]->GetComponentByClass(UTSNetworkedComponent::StaticClass())))
			{
				Data.Type = TheShift::InteractableActionType::OpenDoorRotatePlus;
				Data.Tag = TCHAR_TO_UTF8(*NetworkComp->GetActorTag());
				LOG_INFO("Door Tag: %s", Data.Tag.c_str());

				SendPacket(Data);

				// 한번 열었으면 비활성화한다.
				if (ParentTrigger)
				{
					// 현재 모든 directive가 끝난 후 trigger가 비활성화 되도록 한다.
					ParentTrigger->ScheduleParentTriggerDisable();
					LOG_INFO("Parent trigger scheduled to be disabled");
				}
				break;
			}
		}
	}

	case EInteractableActionType::OpenDoorRotateMinus:
	{
		if (Targets.Num() != 0)
		{
			LOG_INFO("Open door minus");
			TheShift::Serializables::InteractableAct Data;

			// UID를 얻어와서 Data.Value에 넣어준다.
			if (auto NetworkComp = Cast<UTSNetworkedComponent>(Targets[0]->GetComponentByClass(UTSNetworkedComponent::StaticClass())))
			{
				Data.Type = TheShift::InteractableActionType::OpenDoorRotateMinus;
				Data.Tag = TCHAR_TO_UTF8(*NetworkComp->GetActorTag());
				LOG_INFO("Door Id: %s", Data.Tag.c_str());

				SendPacket(Data);

				// 한번 열었으면 비활성화한다.
				if (ParentTrigger)
				{
					// 현재 모든 directive가 끝난 후 trigger가 비활성화 되도록 한다.
					ParentTrigger->ScheduleParentTriggerDisable();
					LOG_INFO("Parent trigger scheduled to be disabled");
				}
				break;
			}
		}
	}
		
	case EInteractableActionType::NextLevel:
	{
		TheShift::Serializables::LevelChangeReqDebug Data;
		Data.Type = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode())->GetNextMapType();
		SendPacket(Data);
		break;
	}

	case EInteractableActionType::OpenChest:
	{
		if(Targets.IsValidIndex(0))
		{
			if(ATSChest* Chest = Cast<ATSChest>(Targets[0]))
			{
				if(auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode()))
				{
					// 1. Widget 생성
					auto NewWidget = Cast<UTSChestInventoryWidget>(CreateWidget(ChestInventoryWidgetClass));

					// 2. 생성한 widget에 chest actor 전달 Target에 등록되어있음
					if(NewWidget)
					{
						NewWidget->PassChestActor(Chest);
						NewWidget->ChestInitEvent.Broadcast();
						LOG_INFO("Casting widget to UTSChestInventoryWidget successed.");
					}
					else
					{
						LOG_ERROR("Cast widget to UTSChestInventoryWidget failed.");
					}
				}
			}
		}
		break;
	}
		
	default:
		break;
	}
}

