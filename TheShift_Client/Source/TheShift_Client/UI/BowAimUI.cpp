﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "BowAimUI.h"
#include "Components/Image.h"
#include "Components/CanvasPanel.h"
#include "Components/MenuAnchor.h"
#include "Components/CanvasPanelSlot.h"

void UBowAimUI::SetDrawSize(float Size)
{
	AimImage = Cast<UImage>(GetWidgetFromName(TEXT("Aim")));
	if (AimImage)
	{
		auto Slot = Cast<UCanvasPanelSlot>(AimImage->Slot);
		if (Slot)
		{
			//UE_LOG(LogTemp, Log, TEXT("SLOT!!!"));
			Slot->SetSize(FVector2D(Size, Size) * 5.f);
			FVector2D ScreenPos = FVector2D::ZeroVector;
			ScreenPos.X = -5.f * Size / 2.f;
			ScreenPos.Y = ScreenPos.X;
	
			Slot->SetPosition(ScreenPos);
		}//AimImage->SetBrushSize(FVector2D(Size, Size)*10.f);
		//AimImage->Brush.ImageSize = FVector2D(Size, Size);
		//UE_LOG(LogTemp, Log, TEXT("BrushSize Set"));
	}
	else

		UE_LOG(LogTemp, Log, TEXT("AimiImage Is NullPointer"));
	

	/*if (CanvasPanel)
	{
		UE_LOG(LogTemp, Log, TEXT("CanvasPanel Size Set"));
	}
	else
		UE_LOG(LogTemp, Log, TEXT("CanvasPanel Is NullPointer"));*/
}

void UBowAimUI::NativeConstruct()
{
	Super::NativeConstruct();
	AimImage = Cast<UImage>(GetWidgetFromName(TEXT("Aim")));
	CanvasPanel = Cast<UCanvasPanel>(GetWidgetFromName(TEXT("Canvas Panel")));
}