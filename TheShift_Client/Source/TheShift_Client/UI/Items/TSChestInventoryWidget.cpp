﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSChestInventoryWidget.h"
#include "Item/TSChest.h"

void UTSChestInventoryWidget::PassChestActor(ATSChest* Chest)
{
	ParentChest = Chest;
	ParentChest->ChestInventoryWidget = this;
}
