// Fill out your copyright notice in the Description page of Project Settings.


#include "TheShift_ClientGameStateBase.h"

ATheShift_ClientGameStateBase::ATheShift_ClientGameStateBase()
{
	bAllowTickBeforeBeginPlay = true;
	PrimaryActorTick.bCanEverTick = true;

}

void ATheShift_ClientGameStateBase::BeginPlay()
{
	//Mutex = GetGameInstance<UTheShiftGameInstance>()->GetMutex();																																																																																			
	//TestMessageQueue = GetGameInstance<UTheShiftGameInstance>()->GetMessageQueue();
	//if (Mutex != nullptr && TestMessageQueue != nullptr)
	//	UE_LOG(LogTemp, Warning, TEXT("Successfully got mutex and queue from game instance!"));
	UE_LOG(LogTemp, Warning, TEXT("GameState: BeginPlay()"));
}

void ATheShift_ClientGameStateBase::Tick(float DeltaSeconds)
{
	// game loop?
	//Mutex->lock();
	//if (!TestMessageQueue->empty())
	//{
	//	int32_t Message = TestMessageQueue->front();
	//	UE_LOG(LogTemp, Warning, TEXT("Message: %d"), Message);
	//}
	//Mutex->unlock();
}

const int32 ATheShift_ClientGameStateBase::GetCutSceneIndex() const
{
	return CurrentCutSceneIndex;
}

void ATheShift_ClientGameStateBase::SetCutSceneIndex(int32 CutSceneIndex)
{
	CurrentCutSceneIndex = CutSceneIndex;
}
