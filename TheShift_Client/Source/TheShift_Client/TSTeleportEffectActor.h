﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/PoseableMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/Character.h"
#include "TSTeleportEffectActor.generated.h"

UCLASS()
class THESHIFT_CLIENT_API ATSTeleportEffectActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATSTeleportEffectActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintCallable, Category = "Character")
		void SetCharacterToCopy(ACharacter* Character);

	void SetDirection(FVector Direction) { DirectionVector = Direction; IsMove = true; }

private:
	//User For Runtime
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effect, Meta = (AllowPrivateAccess = true))
		TArray<UMaterialInstanceDynamic*> Materials;

	//Character To Copy Pose
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effect, Meta = (AllowPrivateAccess = true))
		ACharacter* CharacterToCopy = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Effect, Meta = (AllowPrivateAccess = true))
		UPoseableMeshComponent* PoseableMeshComponent = nullptr;


	UPROPERTY()
		bool IsMove = false;

	UPROPERTY()
		FVector DirectionVector = FVector::ZeroVector;
};
