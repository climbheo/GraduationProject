﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item/TSItemComponent.h"
#include "TSArmorItemComponent.generated.h"

/**
 * 
 */
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THESHIFT_CLIENT_API UTSArmorItemComponent : public UTSItemComponent
{
	GENERATED_BODY()

public:
	UTSArmorItemComponent();
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UArmorItemType ArmorType;

	//UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	//TSubclassOf<ATSArmor> ArmorClass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float DamageDeduction;
};
