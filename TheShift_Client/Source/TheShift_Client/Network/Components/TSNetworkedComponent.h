﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Protocol/Types.h"

#include "Math/Vector.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TSNetworkedComponent.generated.h"

// 서버와 동기화되는 actor에 추가한다.
// 이 컴포넌트를 가지고 있는 actor는 매 프레임 서버로 부터 새로운 업데이트가 있는지 확인한다.
// 서버의 이전 업데이트에서 받은 정보를 저장하고 새로운 업데이트가 오기 전
// 이전 업데이트의 상태로 예측하여 업데이트를 한다. 
// (예/ 이전 프레임에 속도를 받아서 그 속도로 다음 서버통신 시 까지 업데이트를 한다.)

using namespace TheShift;

using namespace TheShift;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THESHIFT_CLIENT_API UTSNetworkedComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTSNetworkedComponent();

	// Contains update info and updated timepoint.
	//TArray<TheShift::UpdateMessage*> Updates;

	uint32 GetUID() const;
	ActorType GetActorType() const;
	FVector GetCurrentSpeed() const;
	const FVector& GetCurrentInterpolationDirectionWithMagnitude() const;
	int GetHp() const;
	bool GetWhetherExport() const;
	const TheShift::Stats& GetStat() const;
	const FString& GetActorTag() const;
	void SetUID(uint32 NewId);
	void SetCurrentSpeed(FVector& NewSpeed);
	void SetHp(int NewHp);
	void SetActorType(ActorType NewType);
	void SetActorTag(const char* NewTag);

	void SetInterpolationDirectionWithMagnitude(FVector& Value);
	void InterpolateLocation(float InterpolationRatio);

private:

	// 이 Actor에 대해 새로운 위치를 서버로부터 받았으면 현재 위치에서 해당 위치로의 vector를 set해준다.
	FVector InterpolationDirectionWithMagnitude;
	// 이 Actor에 대해 새로운 위치를 서버로부터 받았으면 보간을 위해 0.f으로 set해준다.
	float InterpolatedRatio = 1.f;
	// 정상적인 이동이 아닌 teleport로 이동을 했다면 보간의 대상이 아님
	bool Teleported = false;
	bool ShouldInterpolate = false;

protected:
	// Actor's unique identifier
	UPROPERTY(VisibleAnywhere)
	uint32 UID = INVALID_UID;

	// Called when the game starts
	virtual void BeginPlay() override;
	bool NewUpdate;
	ActorType Type;
	ActType LatestAct;
	bool Interpolation;
	int Hp;

	FVector CurrentSpeed;

	TheShift::Stats Stat;

	// Server에서 맵에 배치된 actor들을 구분할때 사용
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Network)
	FString Tag;

	// Whether to export
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Network)
	bool Export = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Network)
	bool Interactable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Network)
	bool Destructible = false;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
