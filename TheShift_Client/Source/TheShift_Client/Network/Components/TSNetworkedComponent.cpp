﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSNetworkedComponent.h"
#include "GameFramework/Actor.h"
#include "TheShift_ClientGameModeBase.h"
#include "Game/Actors/Actor.h"
#include "Protocol/Log.h"
#include "Actors/TSCharacterAnimInstance.h"
#include "Actors/TSPlayer.h"
#include "TheShift_GameInstanceBase.h"

// Sets default values for this component's properties
UTSNetworkedComponent::UTSNetworkedComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...

	CurrentSpeed = FVector::ZeroVector;
}


uint32 UTSNetworkedComponent::GetUID() const
{
	return UID;
}

ActorType UTSNetworkedComponent::GetActorType() const
{
	if (Interactable)
		return ActorType::Interactable;
	if (Destructible)
		return ActorType::Destructible;
	return Type;
}

FVector UTSNetworkedComponent::GetCurrentSpeed() const
{
	return CurrentSpeed;
}

const FVector& UTSNetworkedComponent::GetCurrentInterpolationDirectionWithMagnitude() const
{
	return InterpolationDirectionWithMagnitude;
}

int UTSNetworkedComponent::GetHp() const
{
	return Hp;
}

// 해당 actor의 bounding volume정보를 export할지 여부를 return한다.
bool UTSNetworkedComponent::GetWhetherExport() const
{
	return Export;
}

const TheShift::Stats& UTSNetworkedComponent::GetStat() const
{
	return Stat;
}

const FString& UTSNetworkedComponent::GetActorTag() const
{
	return Tag;
}

void UTSNetworkedComponent::SetUID(uint32 NewId)
{
	UID = NewId;
}

void UTSNetworkedComponent::SetCurrentSpeed(FVector& NewSpeed)
{
	if(NewSpeed.IsNearlyZero())
		NewSpeed = FVector::ZeroVector;
	CurrentSpeed = NewSpeed;
}

void UTSNetworkedComponent::SetHp(int NewHp)
{
	Hp = NewHp;
}

void UTSNetworkedComponent::SetActorType(ActorType NewType)
{
	Type = NewType;
}

void UTSNetworkedComponent::SetActorTag(const char* NewTag)
{
	Tag = NewTag;
}

// 새로운 위치를 받았을 경우 해당 위치로 보간을 위해 보간 관련 정보를 set 해준다.
void UTSNetworkedComponent::SetInterpolationDirectionWithMagnitude(FVector& Value)
{
	ShouldInterpolate = true;

	//활 상태의 플레이어에게 BlendSpace의 이동값 변수를 갱신한다.(자신의 플레이어는 자신이 갱신)/////
	auto PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	ATSPlayer* MyPlayer = nullptr;

	if(PlayerController)
		MyPlayer = Cast<ATSPlayer>(PlayerController->GetPawn());

	if (MyPlayer)
	{
		if (auto Player = Cast<ATSPlayer>(GetOwner()))
		{
			if (Player != MyPlayer)
			{
				if (auto AnimInstance = Player->GetCurrentAnimInstance())
				{
					AnimInstance->GoalHorizonMove = Value.Y;
					AnimInstance->GoalVerticalMove = Value.X;
				}
			}
		}
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////
	InterpolationDirectionWithMagnitude = Value;
	InterpolatedRatio = 0.f;
}

// InterpolationRatio의 비율만큼 보간해준다.
// 직전에 Teleport한 경우 보간은 이뤄지지 않는다.
void UTSNetworkedComponent::InterpolateLocation(float InterpolationRatio)
{
	// Interpolation이 필요할때만 실행한다.
	if(ShouldInterpolate == true)
	{
		// 순간이동 한 경우 보간해주지 않는다.
		if(Teleported == false)
		{
			// 이미 Interpolate 된 비율과 새로 Interpolate 될 비율의 합이 1.f을 넘을 경우
			// InterpolationRatio를 1.f - InterpolatedRatio로 잘라준다.
			if(InterpolationRatio + InterpolatedRatio > 1.f)
			{
				InterpolationRatio = 1.f - InterpolatedRatio;
				ShouldInterpolate = false;
			}

			// InterpolationRatio의 비율만큼 보간한 위치로 이동시킨다.
			auto NewLocation = GetOwner()->GetActorLocation() + InterpolationDirectionWithMagnitude * InterpolationRatio;
			GetOwner()->SetActorLocation(NewLocation);
			auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
			if (UID == GameMode->MyActorId)
			{
				auto CorrespondingActor = GameMode->GetCorrespondingActor(UID);
				if (CorrespondingActor != nullptr)
				{
					CorrespondingActor->SetTranslation(NewLocation.X, NewLocation.Y, NewLocation.Z);
					CorrespondingActor->SetLastTranslation();
				}
				else
				{
					LOG_ERROR("Corresponding actor is nullptr ID: %d", UID);
				}
			}

			// 보간된 비율을 총 보간된 비율에 더해준다.
			InterpolatedRatio += InterpolationRatio;
		}
		else
		{
			// 텔레포트로 이동한 것은 보간하지 않으니 보간 관련 정보를 다시 set해준다.
			InterpolationDirectionWithMagnitude = FVector::ZeroVector;
			Teleported = false;
			ShouldInterpolate = false;
			InterpolatedRatio = 0.f;
		}
	}
}

// Called when the game starts
void UTSNetworkedComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UTSNetworkedComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if(ShouldInterpolate)
	{
		auto GameMode = Cast<ATheShift_ClientGameModeBase>(GetWorld()->GetAuthGameMode());
		InterpolateLocation(DeltaTime / GameMode->GetServerUpdateTimestep());
	}
}