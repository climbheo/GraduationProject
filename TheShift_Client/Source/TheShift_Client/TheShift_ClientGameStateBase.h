// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <queue>
#include <mutex>

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "TheShift_ClientGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API ATheShift_ClientGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	ATheShift_ClientGameStateBase();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

public:
	UFUNCTION(BlueprintCallable, Category = "CutScene")
	const int32 GetCutSceneIndex() const;

	UFUNCTION(BlueprintCallable, Category = "CutScene")
	void SetCutSceneIndex(int32 CutSceneIndex);



private:
	std::queue<int32_t>* TestMessageQueue;
	std::mutex* Mutex;

	//Test Value
	bool isCutScenePlaying = false;
	int32 CurrentCutSceneIndex = 0;
};
