﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Protocol/MemoryStream.h"
#include "Protocol/Serializables.h"
#include "UI/MainMenu/Lobby/SessionInfo.h"
#include "UI/MainMenu/Lobby/PlayerInfo.h"

#include "TheShift_MainMenuGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API ATheShift_MainMenuGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	

public:
	ATheShift_MainMenuGameModeBase();
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
	TSubclassOf<UUserWidget> StartingWidgetClass;

	UPROPERTY()
	UUserWidget* CurrentWidget;

	// Session들에 대한 정보를 가지고 있는다.
	UPROPERTY(BlueprintReadOnly)
	TArray<USessionInfo*> SessionsInLobby;
	// Lobby에 접속해있는 player들의 정보를 가지고 있는다.
	UPROPERTY(BlueprintReadOnly)
	TArray<UPlayerInfo*> PlayersInLobby;
	// 새로운 lobby data를 수신했는지 나타낸다.
	UPROPERTY(BlueprintReadWrite)
	bool LobbyCanUpdate = false;

private:
	class UTheShift_GameInstanceBase* GameInstance = nullptr;
	InputMemoryStream InputStream;

	void ProcessPacket(uint8* Buffer);

	void ProcessConnected(TheShift::Serializables::Connected& Data);
	void ProcessLobbyConnected(TheShift::Serializables::LobbyConnected& Data);
	void ProcessLobbyInfo(TheShift::Serializables::LobbyInfo& Data);
	void ProcessReadyState(TheShift::Serializables::ReadyState& Data);
	void ProcessSessionConnected(TheShift::Serializables::SessionConnected& Data);
	void ProcessMapSelect(TheShift::Serializables::MapSelect& Data);
	void ProcessSessionStarted(TheShift::Serializables::SessionStarted& Data);
	void ProcessChat(TheShift::Serializables::Chat& Data);

public:
	UFUNCTION(BlueprintCallable, Category = "Lobby")
	void LobbyInfoRequest();

	UFUNCTION(BlueprintCallable, Category = "Lobby")
	void LobbyJoinRequest();

	UFUNCTION(BlueprintCallable, Category = "Lobby")
	void SessionJoinRequest(int SessionId);

	UFUNCTION(BlueprintCallable, Category = "Lobby")
	void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

private:


};
