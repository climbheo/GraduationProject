﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/TSMonster.h"
#include "TSWereWolf.generated.h"

/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API ATSWereWolf : public ATSMonster
{
	GENERATED_BODY()
public:
	ATSWereWolf();

protected:
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

public:
	virtual void Tick(float DeltaTime) override;

	void ApplyMeshAndActorType();

	UFUNCTION(BlueprintCallable, Category = Mesh)
		void SetWolfType(int Num);

public:
	void Act(TheShift::ActType ActEnum) override;
	void Act(TheShift::ActType ActEnum, FVector Vector) override;
	void Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId) override;
	void Act(TheShift::ActType ActEnum, int Value) override;
	void OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead) override;

	virtual void ExportAnimData(FString FileName) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh, Meta = (AllowPrivateAccess = true))
		TArray<class USkeletalMesh*> Meshes;

	//Wolf 종류
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = Mesh, Meta = (AllowPrivateAccess = true))
		int MeshNum = 0;

	UPROPERTY()
		class UAnimSequence* DefaultHitAnim;

	UPROPERTY()
		class UAnimMontage* LockOnMontage;

	UPROPERTY()
		class UAnimSequence* Combo2Montage;

	UPROPERTY()
		class UAnimMontage* Combo3Montage;
};
