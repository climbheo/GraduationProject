﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/TSMonster.h"
#include "TSKnightMonster.generated.h"

/**
 *
 */
UCLASS()
class THESHIFT_CLIENT_API ATSKnightMonster : public ATSMonster
{
	GENERATED_BODY()

public:
	ATSKnightMonster();

protected:
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;

public:
	virtual void Tick(float DeltaTime) override;

public:
	void Act(TheShift::ActType ActEnum) override;
	void Act(TheShift::ActType ActEnum, FVector Vector) override;
	void Act(TheShift::ActType ActEnum, TheShift::UID TargetActorId) override;
	void OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead) override;

private:
	UPROPERTY()
	class UAnimSequence* DefaultHitAnim;

	UPROPERTY()
	class UAnimSequence* AttackAnim[2];
};
