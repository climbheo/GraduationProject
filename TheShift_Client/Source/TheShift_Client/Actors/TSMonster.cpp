﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSMonster.h"
#include "TSPlayerAnimInstance.h"
#include "TSWeapon.h"
#include "Components/WidgetComponent.h"
#include "Components/Image.h"
#include "UI/BowAimUI.h"

ATSMonster::ATSMonster()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//NetworkComp->SetActorType(ActorType::Player);
	//NetworkComp->SetActorTag("Monster");

	//Default Mesh
	//static ConstructorHelpers::FObjectFinder<USkeletalMesh> skeletalMesh(TEXT("/Game/Graphic/MainPlayer/Player.Player"));
	//if(skeletalMesh.Succeeded())
	//	GetMesh()->SetSkeletalMesh(skeletalMesh.Object);

	//GetMesh()->SetRelativeScale3D(FVector(2.5f, 2.5f, 2.5f));

	//GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	//static ConstructorHelpers::FClassFinder<UAnimInstance> defaultAnimInstance(TEXT("/Game/Graphic/MainPlayer/AnimBP_Player.AnimBP_Player_C"));
	//if(defaultAnimInstance.Succeeded())
	//	GetMesh()->SetAnimInstanceClass(defaultAnimInstance.Class);

	//캐릭터 속도제한
	//TODO: import한 stat에 따라야 함.
	GetCharacterMovement()->MaxWalkSpeed = 600.f;

	AttackEndComboState();

	CurrentWeaponType = WeaponType::SWORD;

	BowAimUIComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("AIMUI"));
	BowAimUIComponent->SetupAttachment(GetMesh());
	BowAimUIComponent->SetWidgetSpace(EWidgetSpace::Screen);
	BowAimUIComponent->SetHiddenInGame(true);

	//OnDestroyed.AddDynamic(this, &ATSMonster::OnCharacterDestroyed);

	static ConstructorHelpers::FClassFinder<UUserWidget> UI_HUD(TEXT("/Game/Graphic/UI/CharacterUI/BowAimUI_BP.BowAimUI_BP_C"));
	if (UI_HUD.Succeeded())
		AimWidgetClass = UI_HUD.Class;
}

void ATSMonster::SetAimUIDraw(bool IsDraw, float Distance)
{

	BowAimUIComponent->SetHiddenInGame(!IsDraw);
	if (IsDraw)
	{
		BowAimUIComponent->SetWidgetClass(AimWidgetClass);
		BowAimUIComponent->SetRelativeLocation(FVector(0.f, 0.f, 120.f));


		//UE_LOG(LogTemp, Log, TEXT("Distance : %f"), Distance);
		float Size = 120.f * 80.f / Distance;
		//UE_LOG(LogTemp, Log, TEXT("Final Size : %f"), Size);
		if (Size > 30.f)
			Size = 30.f;

		auto BowAimUI = Cast<UBowAimUI>(BowAimUIComponent->GetUserWidgetObject());
		if (BowAimUI)
			BowAimUI->SetDrawSize(Size*1.3f);
		else
			UE_LOG(LogTemp, Log, TEXT("BowAIMUICasting Error"));

		//BowAimUIComponent->SetDrawSize(FVector2D(Size, Size));
	}

	
}
