﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TSDestructible.h"
#include "DestructibleComponent.h"
#include "Protocol/Log.h"

// Sets default values
ATSDestructible::ATSDestructible()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Default Root Component"));
	NetworkComp = CreateDefaultSubobject<UTSNetworkedComponent>(TEXT("Networked Component"));
}

// 
void ATSDestructible::OnHit(float Hp, float Damage, TheShift::ActType AttackType, TheShift::UID AttackerId, bool IsDead)
{
	if(IsDead)
	{
		LOG_INFO("Destruct!");
		// 파괴 처리
		for (auto Child : DestructibleChildren)
		{
			auto DestructibleComponent = Cast<UDestructibleComponent>(Child->GetComponentByClass(UDestructibleComponent::StaticClass()));
			if (DestructibleComponent)
			{
				DestructibleComponent->SetCollisionProfileName("Destructible");
				DestructibleComponent->SetSimulatePhysics(true);
				Child->SetActorEnableCollision(true);
			}
		}

		// Collision을 꺼준다.
		SetActorEnableCollision(false);
	}
}

// Called when the game starts or when spawned
void ATSDestructible::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATSDestructible::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

