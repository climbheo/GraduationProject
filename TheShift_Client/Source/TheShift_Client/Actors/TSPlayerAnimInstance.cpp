// Fill out your copyright notice in the Description page of Project Settings.


#include "TSPlayerAnimInstance.h"
#include "TSPlayer.h"
#include "Animation/AnimInstanceProxy.h"
#include "Animation/AnimNode_SequencePlayer.h"
#include "Animation/AnimNode_StateMachine.h"
#include "Network/Components/TSNetworkedComponent.h"

UTSPlayerAnimInstance::UTSPlayerAnimInstance()
{
	/*static ConstructorHelpers::FObjectFinder<UAnimMontage> DefaultSwordAttackMontage(TEXT("/Game/Graphic/DynamicMesh/GreatSword_Player/AnimBP_Player.AnimBP_Player"));
	if (DefaultSwordAttackMontage.Succeeded())
		SwordAttackMontage = DefaultSwordAttackMontage.Object;*/
}

void UTSPlayerAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	auto Pawn = TryGetPawnOwner();
	if(!::IsValid(Pawn))
		return;

	if(!Character.IsValid())
		Character = Cast<ATSPlayer>(Pawn);

	auto NetWorkComponent = Pawn->GetComponentByClass(UTSNetworkedComponent::StaticClass());
	if(NetWorkComponent != nullptr)
	{
		auto Component = Cast<UTSNetworkedComponent>(NetWorkComponent);
		if(Component != nullptr)
			CurrentSpeed = Component->GetCurrentSpeed().Size2D();
	}
}

void UTSPlayerAnimInstance::PlayAttackMontage()
{
	if(Character.IsValid())
	{
		WeaponType CurrentWeaponType = Character.Get()->GetCurrentWeaponType();
		switch(CurrentWeaponType)
		{
		case WeaponType::SWORD:
		{
			if(!Montage_IsPlaying(SwordAttackMontage))
				Montage_Play(SwordAttackMontage, 1.f);
			break;
		}
		case WeaponType::BOW:
		{

			break;
		}
		}
	}
}

void UTSPlayerAnimInstance::JumpToAttckMontageSection(int32 NewSection)
{
	if(Montage_IsPlaying(SwordAttackMontage))
		Montage_JumpToSection(FName(*FString::Printf(TEXT("Attack%d"), NewSection)), SwordAttackMontage);
	else
	{
		Montage_Play(SwordAttackMontage);
		Montage_JumpToSection(FName(*FString::Printf(TEXT("Attack%d"), NewSection)), SwordAttackMontage);
	}

	//공격이 바뀔때마다 Input관련 변수들을 Init
	Cast<ATSPlayer>(TryGetPawnOwner())->InitDirection();

	UE_LOG(LogTemp, Log, TEXT("%s"), *FName(*FString::Printf(TEXT("Attack%d"), NewSection)).ToString());
}

void UTSPlayerAnimInstance::AnimNotify_NextAttackCheck()
{
	OnNextAttackCheck.Broadcast();
}

void UTSPlayerAnimInstance::AnimNotify_HitCheck()
{
	OnHitCheck.Broadcast();
}

