// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "EngineMinimal.h"
#include "Animation/AnimInstance.h"
#include "TSPlayerAnimInstance.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnNextAttackCheckDelegate);
DECLARE_MULTICAST_DELEGATE(FOnHitCheckDelegate);


/**
 *
 */
UCLASS()
class THESHIFT_CLIENT_API UTSPlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UTSPlayerAnimInstance();
	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

public:
	void PlayAttackMontage();
	void JumpToAttckMontageSection(int32 NewSection);

public:
	//다음 Combo로 넘어갈때 호출되는 Delegate
	FOnNextAttackCheckDelegate OnNextAttackCheck;

	//공격(충돌) 체크를 해야할때 호출되는 Delegate
	FOnHitCheckDelegate OnHitCheck;

private:
	UFUNCTION()
	void AnimNotify_NextAttackCheck();

	UFUNCTION()
	void AnimNotify_HitCheck();

private:
	//NetWorkComp의 Speed를 BlendSpace에 사용하기 위해 받아오는 변수
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pawn, Meta = (AllowPrivateAccess = true))
	float CurrentSpeed = 0.f;

	//Sword공격몽타주
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Attack, Meta = (AllowPrivateAccess = true))
	UAnimMontage* SwordAttackMontage;

	TWeakObjectPtr<class ATSPlayer> Character;



};
