﻿#include "JsonWriter.h"

namespace TheShift
{
	namespace JsonFormatter
	{
		JsonWriter::JsonWriter(const char* path) : Osw(Ofs), Writer(Osw)
		{
			Open(path);

		}

		JsonWriter::JsonWriter() : Osw(Ofs), Writer(Osw)
		{

		}

		bool JsonWriter::Open(const char* path)
		{
			if (Ofs.is_open())
			{
				return true;
			}
			Ofs.open(path, std::fstream::out | std::fstream::trunc);

			if (Ofs.is_open())
			{
				Doc.SetObject();
				return true;
			}
			else
			{
				return false;
			}
		}

		bool JsonWriter::Close()
		{
			if (!Ofs.is_open())
			{
				return true;
			}
			Ofs.close();

			if (Ofs.is_open())
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		void JsonWriter::ExportActorInfo(const char* path, std::vector<ActorInfo>& info)
		{
			if (Open(path))
			{
				WriteActorInfo(info);
			}
		}

		void JsonWriter::ExportMapInfo(const char* path, MapInfo& info)
		{
			if (Open(path))
			{
				WriteMapInfo(info);
			}
		}

		void JsonWriter::ExportAnimNotifyInfo(const char* path, std::vector<AnimNotifyInfo>& info)
		{
			if (Open(path))
			{
				WriteAnimNotifyInfo(info);
			}
		}

		void JsonWriter::WriteActorInfo(std::vector<ActorInfo>& info)
		{
			rapidjson::Value ActorInfoData(rapidjson::kArrayType);
			ActorInfoData.SetArray();
			for (auto& Info : info)
			{
				rapidjson::Value InfoValue(rapidjson::kObjectType);
				InfoValue.SetObject();

				InfoValue.AddMember("Actor Type", static_cast<uint16_t>(Info.Type), Doc.GetAllocator());

				////////////// Stat //////////////
				rapidjson::Value StatValue(rapidjson::kObjectType);
				StatValue.SetObject();

				StatValue.AddMember("Mass", Info.Stat.Mass, Doc.GetAllocator());

				StatValue.AddMember("Forward Run Speed", Info.Stat.ForwardRunSpeed, Doc.GetAllocator());
				StatValue.AddMember("Side Run Speed", Info.Stat.SideRunSpeed, Doc.GetAllocator());
				StatValue.AddMember("Back Run Speed", Info.Stat.BackRunSpeed, Doc.GetAllocator());

				StatValue.AddMember("Forward Walk Speed", Info.Stat.ForwardWalkSpeed, Doc.GetAllocator());
				StatValue.AddMember("Side Walk Speed", Info.Stat.SideWalkSpeed, Doc.GetAllocator());
				StatValue.AddMember("Back Walk Speed", Info.Stat.BackWalkSpeed, Doc.GetAllocator());

				StatValue.AddMember("Forward Run Acceleration", Info.Stat.ForwardRunAcceleration, Doc.GetAllocator());
				StatValue.AddMember("Side Run Acceleration", Info.Stat.SideRunAcceleration, Doc.GetAllocator());
				StatValue.AddMember("Back Run Acceleration", Info.Stat.BackRunAcceleration, Doc.GetAllocator());

				StatValue.AddMember("Forward Walk Acceleration", Info.Stat.ForwardWalkAcceleration, Doc.GetAllocator());
				StatValue.AddMember("Side Walk Acceleration", Info.Stat.SideWalkAcceleration, Doc.GetAllocator());
				StatValue.AddMember("Back Walk Acceleration", Info.Stat.BackWalkAcceleration, Doc.GetAllocator());

				InfoValue.AddMember("Stat", StatValue, Doc.GetAllocator());
				//////////////////////////////////



				ActorInfoData.PushBack(InfoValue, Doc.GetAllocator());
			}

			Doc.AddMember("Actor Info", ActorInfoData.GetArray(), Doc.GetAllocator());
			Doc.Accept(Writer);
		}

		void JsonWriter::WriteMapInfo(MapInfo& info)
		{
			rapidjson::Value MapData(rapidjson::kObjectType);
			MapData.SetObject();
			// Map Type
			MapData.AddMember("Map Type", static_cast<uint16_t>(info.Type), Doc.GetAllocator());

			// Heightmap Name
			rapidjson::Value HeightmapArray(rapidjson::kArrayType);
			HeightmapArray.SetArray();
			for (int i = 0; i < info.Heightmaps.size(); ++i)
			{
				rapidjson::Value HeightmapValue(rapidjson::kObjectType);
				HeightmapValue.SetObject();

				rapidjson::Value StrValue(rapidjson::kStringType);
				StrValue.SetString(info.Heightmaps[i].FileName.c_str(), Doc.GetAllocator());
				HeightmapValue.AddMember("Name", StrValue, Doc.GetAllocator());
				rapidjson::Value PositionValue(rapidjson::kObjectType);
				PositionValue.SetObject();
				PositionValue.AddMember("X", info.Heightmaps[i].Position.X, Doc.GetAllocator());
				PositionValue.AddMember("Y", info.Heightmaps[i].Position.Y, Doc.GetAllocator());
				PositionValue.AddMember("Z", info.Heightmaps[i].Position.Z, Doc.GetAllocator());
				HeightmapValue.AddMember("Position", PositionValue, Doc.GetAllocator());

				rapidjson::Value RotationValue(rapidjson::kObjectType);
				RotationValue.SetObject();
				RotationValue.AddMember("X", info.Heightmaps[i].Rotation.X, Doc.GetAllocator());
				RotationValue.AddMember("Y", info.Heightmaps[i].Rotation.Y, Doc.GetAllocator());
				RotationValue.AddMember("Z", info.Heightmaps[i].Rotation.Z, Doc.GetAllocator());
				HeightmapValue.AddMember("Rotation", RotationValue, Doc.GetAllocator());

				rapidjson::Value ScaleValue(rapidjson::kObjectType);
				ScaleValue.SetObject();
				ScaleValue.AddMember("X", info.Heightmaps[i].Scale.X, Doc.GetAllocator());
				ScaleValue.AddMember("Y", info.Heightmaps[i].Scale.Y, Doc.GetAllocator());
				ScaleValue.AddMember("Z", info.Heightmaps[i].Scale.Z, Doc.GetAllocator());
				HeightmapValue.AddMember("Scale", ScaleValue, Doc.GetAllocator());

				//HeightmapValue.AddMember("Row Size", info.Heightmaps[i].RowSize, Doc.GetAllocator());
				//HeightmapValue.AddMember("Column Size", info.Heightmaps[i].ColumnSize, Doc.GetAllocator());

				//rapidjson::Value RowArray(rapidjson::kArrayType);
				//RowArray.SetArray();
				//for(int r = 0; r < info.Heightmaps[i].Verts.size(); ++r)
				//{
				//	rapidjson::Value VertsArray(rapidjson::kArrayType);
				//	VertsArray.SetArray();
				//	for(int c = 0; c < info.Heightmaps[i].Verts[0].size(); ++c)
				//	{
				//		rapidjson::Value VertValue(rapidjson::kArrayType);
				//		VertValue.PushBack(info.Heightmaps[i].Verts[r][c].X, Doc.GetAllocator());
				//		VertValue.PushBack(info.Heightmaps[i].Verts[r][c].Y, Doc.GetAllocator());
				//		VertValue.PushBack(info.Heightmaps[i].Verts[r][c].Z, Doc.GetAllocator());
				//		VertsArray.PushBack(VertValue, Doc.GetAllocator());
				//	}
				//	RowArray.PushBack(VertsArray, Doc.GetAllocator());
				//}
				//HeightmapValue.AddMember("Verts", RowArray, Doc.GetAllocator());
				HeightmapArray.PushBack(HeightmapValue, Doc.GetAllocator());
			}
			MapData.AddMember("Heightmaps", HeightmapArray, Doc.GetAllocator());

			rapidjson::Value PlacementsData(rapidjson::kArrayType);
			PlacementsData.SetArray();
			for (auto& PlacementInfo : info.ActorPlacements)
			{
				rapidjson::Value PlacementValue(rapidjson::kObjectType);
				PlacementValue.SetObject();

				rapidjson::Value TagValue(rapidjson::kStringType);
				TagValue.SetString(PlacementInfo.Tag.c_str(), Doc.GetAllocator());
				PlacementValue.AddMember("Tag", TagValue, Doc.GetAllocator());

				PlacementValue.AddMember("Actor Type", static_cast<uint16_t>(PlacementInfo.Type), Doc.GetAllocator());

				rapidjson::Value PositionValue(rapidjson::kObjectType);
				PositionValue.AddMember("X", PlacementInfo.ActorPosition.X, Doc.GetAllocator());
				PositionValue.AddMember("Y", PlacementInfo.ActorPosition.Y, Doc.GetAllocator());
				PositionValue.AddMember("Z", PlacementInfo.ActorPosition.Z, Doc.GetAllocator());
				PlacementValue.AddMember("Position", PositionValue, Doc.GetAllocator());

				rapidjson::Value RotationValue(rapidjson::kObjectType);
				RotationValue.AddMember("X", PlacementInfo.ActorRotation.X, Doc.GetAllocator());
				RotationValue.AddMember("Y", PlacementInfo.ActorRotation.Y, Doc.GetAllocator());
				RotationValue.AddMember("Z", PlacementInfo.ActorRotation.Z, Doc.GetAllocator());
				PlacementValue.AddMember("Rotation", RotationValue, Doc.GetAllocator());

				////////////// Bound Info //////////////
				rapidjson::Value BoundInfo(rapidjson::kArrayType);
				BoundInfo.SetArray();

				for (auto& VolumeInfo : PlacementInfo.BoundInfo)
				{
					rapidjson::Value VolumeValue(rapidjson::kObjectType);
					VolumeValue.SetObject();

					VolumeValue.AddMember("Bounding Volume Type", static_cast<uint16_t>(VolumeInfo.BoundingVolumeType_), Doc.GetAllocator());

					// Box에 필요한 Extent
					if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::AABB ||
						VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::OBB)
					{
						rapidjson::Value ExtentVectorValue(rapidjson::kObjectType);
						ExtentVectorValue.AddMember("X", VolumeInfo.ScaledExtent.X, Doc.GetAllocator());
						ExtentVectorValue.AddMember("Y", VolumeInfo.ScaledExtent.Y, Doc.GetAllocator());
						ExtentVectorValue.AddMember("Z", VolumeInfo.ScaledExtent.Z, Doc.GetAllocator());
						VolumeValue.AddMember("Scaled Extent", ExtentVectorValue, Doc.GetAllocator());
					}

					// Capsule에 필요한 Half height, Radius
					else if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::Capsule)
					{
						VolumeValue.AddMember("Half Height", VolumeInfo.HalfHeight, Doc.GetAllocator());
						VolumeValue.AddMember("Radius", VolumeInfo.Radius, Doc.GetAllocator());
					}

					// Sphere에 필요한 Radius
					else if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::Sphere)
					{
						VolumeValue.AddMember("Radius", VolumeInfo.Radius, Doc.GetAllocator());
					}

					rapidjson::Value OriginVectorValue(rapidjson::kObjectType);
					OriginVectorValue.AddMember("X", VolumeInfo.RelativeLocation.X, Doc.GetAllocator());
					OriginVectorValue.AddMember("Y", VolumeInfo.RelativeLocation.Y, Doc.GetAllocator());
					OriginVectorValue.AddMember("Z", VolumeInfo.RelativeLocation.Z, Doc.GetAllocator());
					VolumeValue.AddMember("Relative Location", OriginVectorValue, Doc.GetAllocator());

					rapidjson::Value RotationOffsetVectorValue(rapidjson::kObjectType);
					RotationOffsetVectorValue.AddMember("X", VolumeInfo.RelativeRotation.X, Doc.GetAllocator());
					RotationOffsetVectorValue.AddMember("Y", VolumeInfo.RelativeRotation.Y, Doc.GetAllocator());
					RotationOffsetVectorValue.AddMember("Z", VolumeInfo.RelativeRotation.Z, Doc.GetAllocator());
					VolumeValue.AddMember("Relative Rotation", RotationOffsetVectorValue, Doc.GetAllocator());
					BoundInfo.PushBack(VolumeValue, Doc.GetAllocator());
				}
				PlacementValue.AddMember("Bound Info", BoundInfo, Doc.GetAllocator());
				////////////////////////////////////////

				////////////// Item Info //////////////
				rapidjson::Value InventoryInfo(rapidjson::kArrayType);
				InventoryInfo.SetArray();
				
				for(auto& Item : PlacementInfo.InventoryInfo)
				{
					rapidjson::Value ItemValue(rapidjson::kObjectType);
					ItemValue.SetObject();
					ItemValue.AddMember("Index", Item.Index, Doc.GetAllocator());
					ItemValue.AddMember("Category", Item.ItemCategory, Doc.GetAllocator());
					ItemValue.AddMember("Type", Item.ItemType, Doc.GetAllocator());
					InventoryInfo.PushBack(ItemValue, Doc.GetAllocator());
				}
				PlacementValue.AddMember("Inventory Info", InventoryInfo, Doc.GetAllocator());
				////////////////////////////////////////

				PlacementValue.AddMember("Is Movable", PlacementInfo.IsMovable, Doc.GetAllocator());

				PlacementsData.PushBack(PlacementValue, Doc.GetAllocator());
			}
			MapData.AddMember("Actor Placement Info", PlacementsData, Doc.GetAllocator());

			rapidjson::Value ActorTypeData(rapidjson::kArrayType);
			ActorTypeData.SetArray();
			for (auto& ActorTypeInfo : info.ActorTypeInfo)
			{
				rapidjson::Value ActorTypeValue(rapidjson::kObjectType);
				ActorTypeValue.SetObject();

				ActorTypeValue.AddMember("Actor Type", static_cast<uint16_t>(ActorTypeInfo.Type), Doc.GetAllocator());

				////////////// Stat //////////////
				rapidjson::Value StatValue(rapidjson::kObjectType);
				StatValue.SetObject();

				StatValue.AddMember("Mass", ActorTypeInfo.Stat.Mass, Doc.GetAllocator());

				StatValue.AddMember("Forward Run Speed", ActorTypeInfo.Stat.ForwardRunSpeed, Doc.GetAllocator());
				StatValue.AddMember("Side Run Speed", ActorTypeInfo.Stat.SideRunSpeed, Doc.GetAllocator());
				StatValue.AddMember("Back Run Speed", ActorTypeInfo.Stat.BackRunSpeed, Doc.GetAllocator());

				StatValue.AddMember("Forward Walk Speed", ActorTypeInfo.Stat.ForwardWalkSpeed, Doc.GetAllocator());
				StatValue.AddMember("Side Walk Speed", ActorTypeInfo.Stat.SideWalkSpeed, Doc.GetAllocator());
				StatValue.AddMember("Back Walk Speed", ActorTypeInfo.Stat.BackWalkSpeed, Doc.GetAllocator());

				StatValue.AddMember("Forward Run Acceleration", ActorTypeInfo.Stat.ForwardRunAcceleration, Doc.GetAllocator());
				StatValue.AddMember("Side Run Acceleration", ActorTypeInfo.Stat.SideRunAcceleration, Doc.GetAllocator());
				StatValue.AddMember("Back Run Acceleration", ActorTypeInfo.Stat.BackRunAcceleration, Doc.GetAllocator());

				StatValue.AddMember("Forward Walk Acceleration", ActorTypeInfo.Stat.ForwardWalkAcceleration, Doc.GetAllocator());
				StatValue.AddMember("Side Walk Acceleration", ActorTypeInfo.Stat.SideWalkAcceleration, Doc.GetAllocator());
				StatValue.AddMember("Back Walk Acceleration", ActorTypeInfo.Stat.BackWalkAcceleration, Doc.GetAllocator());

				ActorTypeValue.AddMember("Stat", StatValue, Doc.GetAllocator());
				//////////////////////////////////

				////////////// Bound Info //////////////
				rapidjson::Value BoundInfo(rapidjson::kArrayType);
				BoundInfo.SetArray();

				for (auto& VolumeInfo : ActorTypeInfo.BoundInfo)
				{
					rapidjson::Value VolumeValue(rapidjson::kObjectType);
					VolumeValue.SetObject();

					VolumeValue.AddMember("Bounding Volume Type", static_cast<uint16_t>(VolumeInfo.BoundingVolumeType_), Doc.GetAllocator());

					// Box에 필요한 Extent
					if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::AABB ||
						VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::OBB)
					{
						rapidjson::Value ExtentVectorValue(rapidjson::kObjectType);
						ExtentVectorValue.AddMember("X", VolumeInfo.ScaledExtent.X, Doc.GetAllocator());
						ExtentVectorValue.AddMember("Y", VolumeInfo.ScaledExtent.Y, Doc.GetAllocator());
						ExtentVectorValue.AddMember("Z", VolumeInfo.ScaledExtent.Z, Doc.GetAllocator());
						VolumeValue.AddMember("Scaled Extent", ExtentVectorValue, Doc.GetAllocator());
					}

					// Capsule에 필요한 Half height, Radius
					else if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::Capsule)
					{
						VolumeValue.AddMember("Half Height", VolumeInfo.HalfHeight, Doc.GetAllocator());
						VolumeValue.AddMember("Radius", VolumeInfo.Radius, Doc.GetAllocator());
					}

					// Sphere에 필요한 Radius
					else if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::Sphere)
					{
						VolumeValue.AddMember("Radius", VolumeInfo.Radius, Doc.GetAllocator());
					}

					rapidjson::Value OriginVectorValue(rapidjson::kObjectType);
					OriginVectorValue.AddMember("X", VolumeInfo.RelativeLocation.X, Doc.GetAllocator());
					OriginVectorValue.AddMember("Y", VolumeInfo.RelativeLocation.Y, Doc.GetAllocator());
					OriginVectorValue.AddMember("Z", VolumeInfo.RelativeLocation.Z, Doc.GetAllocator());
					VolumeValue.AddMember("Relative Location", OriginVectorValue, Doc.GetAllocator());

					rapidjson::Value RotationOffsetVectorValue(rapidjson::kObjectType);
					RotationOffsetVectorValue.AddMember("X", VolumeInfo.RelativeRotation.X, Doc.GetAllocator());
					RotationOffsetVectorValue.AddMember("Y", VolumeInfo.RelativeRotation.Y, Doc.GetAllocator());
					RotationOffsetVectorValue.AddMember("Z", VolumeInfo.RelativeRotation.Z, Doc.GetAllocator());
					VolumeValue.AddMember("Relative Rotation", RotationOffsetVectorValue, Doc.GetAllocator());
					BoundInfo.PushBack(VolumeValue, Doc.GetAllocator());
				}
				ActorTypeValue.AddMember("Bound Info", BoundInfo, Doc.GetAllocator());
				////////////////////////////////////////

				////////////// Is Movable //////////////
				ActorTypeValue.AddMember("Is Movable", ActorTypeInfo.IsMovable, Doc.GetAllocator());
				////////////////////////////////////////

				ActorTypeData.PushBack(ActorTypeValue, Doc.GetAllocator());
			}

			MapData.AddMember("Actor Type Info", ActorTypeData, Doc.GetAllocator());

			Doc.AddMember("Map Info", MapData, Doc.GetAllocator());
			Doc.Accept(Writer);
		}


		/////////////AnimNotify Export Function////////////
		void JsonWriter::WriteAnimNotifyInfo(std::vector<AnimNotifyInfo>& info)
		{
			rapidjson::Value AnimNotifyInfoData(rapidjson::kArrayType);
			AnimNotifyInfoData.SetArray();
			for (auto& Info : info)
			{
				rapidjson::Value InfoValue(rapidjson::kObjectType);
				InfoValue.SetObject();

				////////////// AnimName   //////////////
				rapidjson::Value StrValue(rapidjson::kStringType);
				StrValue.SetString(Info.AnimName.c_str(), Doc.GetAllocator());
				InfoValue.AddMember("Anim Name", StrValue, Doc.GetAllocator());

				////////////// Notify Info //////////////
				rapidjson::Value NotifyInfo(rapidjson::kArrayType);
				NotifyInfo.SetArray();

				for (auto& Notify : Info.AnimNotifies)
				{
					rapidjson::Value NotifyValue(rapidjson::kObjectType);
					NotifyValue.SetObject();

					//Notify Name
					rapidjson::Value NotifyNameValue(rapidjson::kStringType);
					NotifyNameValue.SetString(Notify.first.c_str(), Doc.GetAllocator());
					NotifyValue.AddMember("Notify Name", NotifyNameValue, Doc.GetAllocator());

					//Notify Trigger Time
					NotifyValue.AddMember("Trigger Time", Notify.second, Doc.GetAllocator());

					NotifyInfo.PushBack(NotifyValue, Doc.GetAllocator());
				}
				InfoValue.AddMember("Notifies", NotifyInfo, Doc.GetAllocator());

				AnimNotifyInfoData.PushBack(InfoValue, Doc.GetAllocator());
			}
			Doc.AddMember("AnimNotify Info", AnimNotifyInfoData.GetArray(), Doc.GetAllocator());
			Doc.Accept(Writer);
		}
	}
}

