#pragma once

#include <numeric>
#include <atomic>
#include <cassert>
#include <mutex>
#include <vector>


template <std::size_t N>
struct is_power_of_two
{
	static const bool value = (N & (N - 1)) == 0;
};

template <std::size_t N>
constexpr auto IsPowerOfTwo = is_power_of_two<N>::value;

// 사이즈는 2의n승만 가능. 마스크할때 필요하기 때문
// PoolSize가 2의n승인지 체크 해주자. (2 4 8 16 32 64...)
// 실제 버퍼의 크기는 PoolSize - 1
// 값 하나는 reserve 해둠.
template <class T, size_t PoolSize = 1024>
class MemoryPool
{
	static_assert(IsPowerOfTwo<PoolSize>&& PoolSize > 2, "MemoryPool size needs to be power of 2.(At least 4)");
private:
	std::vector<T> mBuffer;
	std::vector<uint16_t> mFreeList; // 0부터 쭉 채워놔야 함.
	std::atomic_uint16_t mTail, mHead; // 초기값 tail = 0, head = 1 ? 0xffff로 마스크할건데 0xffff를 reserve 해놓을거니 index하나가 비어있게됨.
	std::atomic_int32_t mCount; // 초기값 PoolSize - 1
	static const uint16_t mMask = PoolSize - 1; // 2의 n승 - 1
	MemoryPool<T, PoolSize>* mNextPool;
	std::mutex mExtendLock;
	size_t mPoolNumber;

public:
	MemoryPool(size_t poolNumber = 0) :
		mTail(0),
		mHead(1),
		mCount(PoolSize - 1),
		mNextPool(nullptr),
		mPoolNumber(poolNumber)
	{
		mBuffer.resize(PoolSize - 1);
		mFreeList.resize(PoolSize);
		// mFreeList는 0부터 쭉 채워놓자.
		std::iota(&mFreeList[1], &mFreeList[PoolSize - 1], 0);

		/*spdlog::info("New memory pool allocated. Element buffer size: {0}, Index buffer size: {1}.",
					 mBuffer.size() * sizeof(T), mFreeList.size() * sizeof(uint16_t));*/
	}

	// 해당 메모리블럭의 인덱스가 필요 없는경우 사용.
	T* alloc()
	{
		// 할당 가능한 메모리가 없으면 새로 만들어줌.
		// 새로만든 풀에서 alloc() 한걸 리턴함.
		if(mCount <= 0)
		{
			return allocExtend();
		}

		int32_t count = atomicDecr(mCount);
		if(mCount <= 0)
		{
			atomicIncr(mCount); // undo.
			return allocExtend();
		}

		uint16_t head = atomicIncr(mHead);
		uint16_t& index = mFreeList[head];
		while(index == 0xffff)
		{
		}

		T* mem = &mBuffer[index];
		// 이 인덱스가 가리키는 메모리가 사용중임을 표시.
		index = 0xffff;

		return mem;
	}

	// 해당 메모리블럭의 인덱스가 필요한 경우에 사용.
	size_t alloc(T*& memory)
	{
		// 할당 가능한 메모리가 없으면 새로 만들어줌.
		// 새로만든 풀에서 alloc() 한걸 리턴함.
		if(mCount <= 0)
		{
			return allocExtend(memory);
		}

		int32_t count = atomicDecr(mCount);
		if(mCount <= 0)
		{
			atomicIncr(mCount); // undo.
			return allocExtend(memory);
		}

		uint16_t head = atomicIncr(mHead);
		uint16_t& index = mFreeList[head];
		while(index == 0xffff)
		{
		}

		memory = &mBuffer[index];
		// 해당 메모리의 index를 리턴하기 위해 저장.
		size_t returnVal = index + (PoolSize - 1) * mPoolNumber;
		// 이 인덱스가 가리키는 메모리가 사용중임을 표시.
		index = 0xffff;

		return returnVal;
	}

	void dealloc(T* mem)
	{
		// 해당 메모리가 담긴 풀을 찾고 거기서 해제해준다.
		if(mem < &mBuffer.front() || mem >& mBuffer.back())
		{
			if(mNextPool)
			{
				mNextPool->dealloc(mem);
			}
			return;
		}

		// 해제하려는 메모리의 index를 구합니다.
		uint16_t index = static_cast<uint16_t>(mem - &mBuffer.front());

		uint16_t tail = atomicIncr(mTail);
		mFreeList[tail] = index;

		atomicIncr(mCount);
	}

	T* unsafeAt(size_t index)
	{
		// index가 범위를 넘어섰는데 mNextPool이 nullptr이라면 access violation.
		if(index > PoolSize - 2)
		{
			if(mNextPool == nullptr)
			{
				return nullptr;
			}
			unsafeAtExtend(index - (PoolSize - 1));
		}


		return &mBuffer[index];
	}

	void clear()
	{
		if(mNextPool != nullptr)
		{
			mNextPool->clear();
		}
		mTail = 0;
		mHead = 1;
		mCount = PoolSize - 1;

		mNextPool = nullptr;
		mPoolNumber = 0;
		std::iota(&mFreeList[1], &mFreeList[PoolSize - 1], 0);
	}

private:
	T* allocExtend()
	{
		// 추가로 생성된 풀이 있으면 그 풀에서 할당받는다.
		if(mNextPool == nullptr)
		{
			// 없으면 락걸고 새로 만들고 거기서 할당받는다.
			{
				// 공간이 부족할때 새로운 풀이 여러번 생기는걸 방지하기 위해 락을 사용한다.
				mExtendLock.lock();
				if(mNextPool == nullptr)
				{
					mNextPool = new MemoryPool<T, PoolSize>(mPoolNumber + 1);
					//spdlog::warn("New memory pool created. Consider modifying pool size.");
				}
				mExtendLock.unlock();
			}
			if(mNextPool == nullptr)
			{
				return nullptr;
			}
		}
		return mNextPool->alloc();
	}

	size_t allocExtend(T*& memory)
	{
		// 추가로 생성된 풀이 있으면 그 풀에서 할당받는다.
		if(mNextPool == nullptr)
		{
			// 없으면 락걸고 새로 만들고 거기서 할당받는다.
			{
				// 공간이 부족할때 새로운 풀이 여러번 생기는걸 방지하기 위해 락을 사용한다.
				mExtendLock.lock();
				if(mNextPool == nullptr)
				{
					mNextPool = new MemoryPool<T, PoolSize>(mPoolNumber + 1);
					//spdlog::warn("New memory pool created. Consider modifying pool size.");
				}
				mExtendLock.unlock();
			}
			assert(mNextPool == nullptr);
			if(mNextPool == nullptr)
			{
				memory = nullptr;
				return 0;
			}
		}
		return mNextPool->alloc(memory);
	}

	T* unsafeAtExtend(size_t index)
	{
		return mNextPool->unsafeAt(index - PoolSize);
	}

	uint16_t atomicIncr(std::atomic_uint16_t& index)
	{
		uint16_t returnVal = index.fetch_add(1);
		index &= mMask;
		return returnVal;
	}

	int32_t atomicIncr(std::atomic_int32_t& count)
	{
		int32_t returnVal = count.fetch_add(1);
		return returnVal;
	}

	int32_t atomicDecr(std::atomic_int32_t& count)
	{
		int32_t returnVal = count.fetch_sub(1);
		return returnVal;
	}
};
