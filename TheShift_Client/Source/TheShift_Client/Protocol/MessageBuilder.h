#pragma once

#include "Serializer.h"

#include <memory>
#include <typeinfo>

namespace TheShift
{
struct Message
{
	ClientId From; // Sender's Id
	uint16_t Dest; // Receiver's Id
	//Serializables::Type TypeOfMessage; // Use (class enum : int) here.
	std::shared_ptr<Serializables::SerializableData> Data; // Actual data.
};

// template<typename DestType>
using MessagePtr = std::shared_ptr<Message>;

// template<typename DestType>
class MessageBuilder
{
public:
	MessageBuilder()
	{}

	// Message에 대한 shared_ptr를 할당하여 return한다.
	template <typename DataType>
	std::shared_ptr<Message> BuildMessage()
	{
		static_assert(std::is_base_of<Serializables::SerializableData, DataType>::value,
					  "MessageBuilder: Can't build a message with invalid data type.");
		MessagePtr NewMessage = std::make_shared<Message>();
		NewMessage->Data = std::make_shared<DataType>();
		return NewMessage;
	}

	template <typename DataType>
	std::shared_ptr<Message> BuildMessage(char* SerializedData)
	{
		static_assert(std::is_base_of<Serializables::SerializableData, DataType>::value,
					  "MessageBuilder: Can't build a message with invalid data type.");
		MessagePtr NewMessage = std::make_shared<Message>();
		std::shared_ptr<DataType> NewData = std::make_shared<DataType>();
		//NewMessage->TypeOfMessage = static_cast<Serializables::Type>(NewData->ClassId());
		NewMessage->Data = NewData;
		DataDeserializer.Deserialize<DataType>(*NewData.get(), SerializedData);
		return NewMessage;
	}

	template <typename DataType>
	std::shared_ptr<DataType> BuildData(char* Data)
	{
		// shared pointer로 데이터 만들고 반환해준다.
		std::shared_ptr<DataType> NewData = std::make_shared<DataType>();
		DataDeserializer.Deserialize(*NewData.get(), Data);
		return NewData;
	}

	// 직렬화된 데이터의 포인터와 그 크기를 return한다.
	void BuildSerializedBuffer(std::shared_ptr<Serializables::SerializableData> SrcData, char* DestBuffer, size_t& Size)
	{
		// 직렬화 한다.
		WriteDataHeader(SrcData);
		DataSerializer.Serialize(SrcData.get(), Size);

		// 직렬화 한 버퍼를 return한다.
		memcpy(DestBuffer, DataSerializer.GetBuffer(), Size);
	}

	void BuildSerializedBuffer(const Serializables::SerializableData& SrcData, char* DestBuffer, size_t& Size)
	{
		WriteDataHeader(SrcData);
		DataSerializer.Serialize(&SrcData, Size);

		memcpy(DestBuffer, DataSerializer.GetBuffer(), Size);
	}

private:
	Serializer DataSerializer;
	Deserializer DataDeserializer;

	// Data의 header를 쓴다.
	void WriteDataHeader(std::shared_ptr<Serializables::SerializableData> Data)
	{
		// header를 작성한다. 1. size, 2. type
		uint32_t Size = 0;
		DataSerializer.Reset();
		DataSerializer.Push(Size);
		DataSerializer.Push(Data->ClassId());
		// DataSerializer.Push(Serializables::SerializableData::GetType(Data.get()));
	}

	void WriteDataHeader(const Serializables::SerializableData& Data)
	{
		uint32_t Size = 0;
		DataSerializer.Reset();
		DataSerializer.Push(Size);
		DataSerializer.Push(Data.ClassId());
	}
};
} // namespace TheShift
