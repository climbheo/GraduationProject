﻿#pragma once


#include "Protocol/Protocol.h"
#include "Protocol/Types.h"

#include <chrono>
#include <map>
#include <queue>

#ifdef TSSERVER
#include "Player.h"
#include "Game/System/Network/SendRequirements.h"
#include "Game/System/TimerQueue.h"
#include "Protocol/MessageBuilder.h"
#endif

#ifdef TSCLIENT
#include "Containers/Map.h"
#include "Misc/InputInfo.h"
class UTSNetworkedComponent;
class AActor;
#endif

namespace TheShift
{
namespace Serializables {
struct ActorPosition;
}

class BoundingVolume;
class Heightmap;
class Actor;
class CollisionResolver;

/// <summary>
/// Game simulation에 필요한 모든 정보를 가진다.
/// Update함수로 게임의 모든 정보를 업데이트한다.
/// </summary>
/// TODO: 생성시 JSON파일로부터 Actor들의 생성 좌표를 읽어와야 한다. Actor의 Type도.
class GameState
{
public:
	GameState();

	bool Update(std::chrono::nanoseconds deltaTime);

#ifdef TSSERVER
	void ApplyMessage(Message* message);
	std::vector<std::shared_ptr<Serializables::SerializableData>>* GetDataToBeSent();
	void ClearDataToBeSent();
	void AddNewPlayer(std::shared_ptr<Avatar> player);
	std::map<ClientId, std::shared_ptr<Avatar>>* GetPlayers();
	MapType GetMapType() const;
	void StartGame();
	//void AddAct(Serializables::Act& act);
	void AddToTimer(std::shared_ptr<TimerCallee> Callee);
	void BroadcastPacket(Serializables::SerializableData& Data);
	void SpawnActorsWithTagAndBroadcast(const char* Tag);

	void SetUpdateTimestep(float NewTimestep);
#endif

	std::map<UID, std::shared_ptr<Actor>>* GetActors();
	std::shared_ptr<Actor> GetActor(UID Id);
	const std::vector<std::shared_ptr<Heightmap>>& GetHeightmaps() const;

	void Init(MapType map);
	void LoadMap(MapType map);

	const std::map<UID, std::shared_ptr<Actor>>* GetActors() const;
	std::list<std::shared_ptr<Actor>> GetActorsByType(ActorType Type);
	std::list<std::shared_ptr<Actor>> GetActorsByTypes(ActorType* Types, int TypeNum);
	CollisionResolver* GetCollisionResolver();

	UID SpawnActor(ActorType Type, const Vector3f& Position);
	UID SpawnActor(ActorType Type, const Eigen::Vector3f& Position);
	std::shared_ptr<Actor> CreateActor(ActorType Type, const Vector3f& Position, UID Id);
	std::shared_ptr<Actor> CreateActor(ActorType Type, const Eigen::Vector3f& Position, UID Id);

	void DestroyActor(UID ActorId);
	void DestroyActor(std::shared_ptr<Actor> ActorToDestroy);

#ifdef TSCLIENT
	// GameState에서 UE Actor를 읽을 수 있도록 포인터로 가지고 있음.
	void RegisterNetworkComps(TMap<UID, UTSNetworkedComponent*>* Container);
	AActor* GetUEActor(UID Id);
	UID GetMyId() const;
	std::shared_ptr<TheShift::Actor> GetCorrespondingActor(UID Id);
	void SetMyActorId(UID Id);
	// Server측에서 처리가 되지 않은 Input들을 처리해 현재 위치와 보간시켜준다.
	void ApplyUnprocessedInput(const Serializables::ActorPosition& PositionInfo, 
										  const std::vector<InputInfo>& UnprocessedInputs, 
										  UID MyCharacterId, 
										  TimePoint LastProcessedTimePoint);
	void SetReconciliationDuration(float DurationInSeconds);
#endif


private:
	std::chrono::nanoseconds TimePassed;
	MapType Map;

	//std::chrono::nanoseconds CollisionDebugDuration;


#ifdef TSSERVER
	bool AllPlayerLoaded = false;

	// Update interval
	float UpdateTimestep;

	// Game simulation에 필요한 모든 정보
	std::map<ClientId, std::shared_ptr<Avatar>> Players;
	std::queue<Serializables::Act> Acts;
	TimerQueue JobTimer;
	std::vector<std::shared_ptr<Serializables::SerializableData>> DataToBeSent;
#endif

#ifdef TSCLIENT
	UID MyActorId = INVALID_UID;
	std::shared_ptr<Actor> DummyActor = nullptr;
	bool MapLoaded = false;
	bool ReconciliationAvailable = false;
	Eigen::Vector3f ReconciliationDirection;
	float ReconciliationLength;
	std::chrono::nanoseconds ReconciliationDuration;
	std::chrono::nanoseconds ReconciliationDelta;
	TMap<UID, UTSNetworkedComponent*>* NetworkComps;
#endif

	std::map<UID, std::shared_ptr<Actor>> Actors;
	std::multimap<std::string, UID> Interactables;

	std::multimap<std::string, ActorPlacementInfo> PlacementInfo;

	std::map<ActorType, ActorInfo> ActorStat;

	std::vector<std::shared_ptr<Heightmap>> Heightmaps;

	CollisionResolver* CollisionResolver_;

	// 몇번째 Start point가 비어있는지 알기 위해
	// Key가 INVALID_CLIENTID이면 비어있는 것
	std::map<ClientId, int> StartPointIndices;

#ifdef TSSERVER
	void ProcessSessionExit(Message* message);
	void ProcessLoadCompleted(Message* message);
	void ProcessInput(Message* message);
	void ProcessSwapWeapon(Message* message);
	void ProcessSessionJoinReq(Message* message);

	Serializables::SessionStarted BuildSessionStarted() const;
	UID InitPlayer(ActorType Type, ClientId Id);
#endif

	UID CreateCharacter(ActorType Type, ClientId id);
	bool SetBoundingVolume(std::shared_ptr<Actor> TargetActor);
	UID GetAvailableUID(bool Reverse = false);

};
}


