﻿#pragma once

#ifndef TS_CHARACTER_H
#define TS_CHARACTER_H

#include "Game/Actors/Actor.h"
#ifdef TSCLIENT
#include "../../Actors/TSWeapon.h"
#endif

namespace TheShift
{
class Character : public Actor
{
public:
	Character(UID id);
	void OnInput(Serializables::Input inputValue) override;

public:
	void ChangeWeaponType(WeaponType Type);
	void LockInput();
	void UnLockInput();
private:
	void AttackStartComboState();
	void AttackEndComboState();

private:
	//Current Weapon Type
	WeaponType CurrentWeaponType = WeaponType::EQUIP_NONE;

	//Current Combo
	int32_t CurrentCombo = 0;

	//Max Combo
	int32_t MaxCombo = 4;

	//Is Attack?		
	bool IsAttacking = false;

	//CanNextCombo? -> CurrentCombo < MaxCombo?
	bool CanNextCombo = false;

	//Is Input incomed while Attacking? 
	bool IsComboInputOn = false;

private:
	//SWORD Variables



};
}
#endif
