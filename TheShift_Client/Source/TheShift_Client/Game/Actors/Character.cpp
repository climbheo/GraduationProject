﻿#include "Game/Actors/Character.h"
#include "Game/System/GameState.h"
#include "Game/Actors/Trigger.h"
#include "Protocol/Log.h"

namespace TheShift
{
Character::Character(UID id) : Actor(id, ActorType::Player)
{
	MovementComponent = std::make_shared<Movement>(&WorldTransform, &CurrentStatus, Stat);
	Hp = 3000.f;
}

void Character::OnInput(Serializables::Input inputValue)
{
	Actor::OnInput(inputValue);

	MovementCheck(inputValue);
	switch(inputValue.InputValue)
	{
	case InputType::MouseLeft_Pressed:
	{
		if(!IsHit && CurrentCombo <= 4)
		{
			if(!IsAttacking)
			{
				AttackStartComboState();
				IsAttacking = true;

				//클라이언트로 보내야 할 정보는? -> CurrentCombo

			#ifdef TSSERVER
				Serializables::Act PlayerAct;
				PlayerAct.Self = Id;
				PlayerAct.Type = static_cast<TheShift::ActType>(CurrentCombo);
				CurrentGameState->BroadcastPacket(PlayerAct);
			#endif

				//spdlog::info("Attack Start : {0}", CurrentCombo);
			}
			//공격중이라면
			else
			{
				if(CanNextCombo)
					IsComboInputOn = true;

				//spdlog::info("Input While Attacking");
			}
		}
		break;
	}
	case InputType::MouseRight_Pressed:
	{
		if(!IsHit && IsAttacking && CurrentCombo <= 4)
		{
			auto lookVector = WorldTransform.GetLookVector();

			lookVector.x() *= 150.f;
			lookVector.y() *= 150.f;
			lookVector.z() *= 150.f;
			MovementComponent->AddImpulse(lookVector, 1.f);

		#ifdef TSSERVER
			Serializables::Act PlayerAct;
			PlayerAct.Self = Id;
			PlayerAct.Type = static_cast<TheShift::ActType>(4 + CurrentCombo);
			CurrentGameState->BroadcastPacket(PlayerAct);
			CurrentCombo += 4;
		#endif
		}
		break;
	}
	case InputType::Combo_Check:
	{
		if(CurrentCombo <= 4)
		{
			CanNextCombo = false;

			if(IsComboInputOn)
			{
				AttackStartComboState();

			#ifdef TSSERVER
				Serializables::Act PlayerAct;
				PlayerAct.Self = Id;
				PlayerAct.Type = static_cast<TheShift::ActType>(CurrentCombo);
				CurrentGameState->BroadcastPacket(PlayerAct);
			#endif
				//spdlog::info("Notify : {0}", CurrentCombo);
			}
		}
		else
		{
			IsAttacking = false;
			AttackEndComboState();

		}

		break;
	}
	case InputType::Combo_End:
	{
		IsAttacking = false;
		AttackEndComboState();
		//spdlog::info("Combo Ended : {0}", CurrentCombo);
		break;
	}
	case InputType::Hit_Check:
	{
		LOG_DEBUG("Hit Check!!");
		auto TriggerId = CurrentGameState->SpawnActor(ActorType::Trigger, Eigen::Vector3f(0.f, 0.f, 0.f));
		auto TriggerActor = std::reinterpret_pointer_cast<Trigger>((*CurrentGameState->GetActors())[TriggerId]);
		auto MyLocation = WorldTransform.GetWorldTranslation();
		auto MyRotation = WorldTransform.GetWorldRotation();
		TriggerActor->SetTranslation(MyLocation.x(), MyLocation.y(), MyLocation.z());
		TriggerActor->Rotate(MyRotation);

		auto TriggerLocation = TriggerActor->GetTransform().GetWorldTranslation();

		// 셋팅 방법
		// 1. Damage 설정
		//TriggerActor->SetAsDamager(100.f);

		// 2. Volume 추가
		//auto NewTriggerAABB = TriggerActor->AddTriggerVolume<AABB>();
		//auto NewTriggerOBB = TriggerActor->AddTriggerVolume<OBB>();
		//auto NewTriggerSphere = TriggerActor->AddTriggerVolume<BoundingSphere>();
		//auto NewTriggerCapsule = TriggerActor->AddTriggerVolume<BoundingCapsule>();

		// 3. Volume 위치, 크기 설정
		//NewTriggerOBB->SetExtent(Eigen::Vector3f(100.f, 100.f, 100.f));
		//NewTriggerOBB->SetRelativeLocation(Eigen::Vector3f(100.f, 0.f, 100.f));

		TriggerActor->SetAsAttack(80.f, -1.f, static_cast<TheShift::ActType>(CurrentCombo), CurrentGameState->GetActor(Id));
		auto NewTriggerOBB = TriggerActor->AddTriggerVolume<OBB>();
		NewTriggerOBB->SetExtent(Eigen::Vector3f(100.f, 100.f, 100.f));
		NewTriggerOBB->SetRelativeLocation(Eigen::Vector3f(100.f, 0.f, 100.f));

		auto WorldTranslation = NewTriggerOBB->GetTransform().GetWorldTranslation();
		LOG_DEBUG("OBBTrigger Location: %f, %f, %f", WorldTranslation.x(), WorldTranslation.y(), WorldTranslation.z());


		//spdlog::info("HitCheck Came!");
		//switch(static_cast<TheShift::ActType>(CurrentCombo))
		//{
		//case ActType::BaseAttack_1:

		//	break;

		//case ActType::BaseAttack_2:
		//	break;

		//case ActType::BaseAttack_3:
		//	break;

		//case ActType::BaseAttack_4:
		//	break;

		//default:
		//	break;
		//}
		break;
	}

	default:
		break;
	}
}

void Character::ChangeWeaponType(WeaponType Type)
{
	if(Type != CurrentWeaponType)
	{
		CurrentWeaponType = Type;

		switch(Type)
		{
		case WeaponType::SWORD:
		{

			break;
	}
		case WeaponType::BOW:
		{

			break;
		}
		case WeaponType::EQUIP_NONE:
		{

			break;
		}
}
}
}

void Character::LockInput()
{
	MovementComponent->LockInput();
}

void Character::UnLockInput()
{
	MovementComponent->UnlockInput();
}

void Character::AttackStartComboState()
{
	CanNextCombo = true;
	IsComboInputOn = false;
	CurrentCombo = (CurrentCombo >= 0 && CurrentCombo < MaxCombo) ? (CurrentCombo + 1) : MaxCombo;

	auto lookVector = WorldTransform.GetLookVector();


	switch(CurrentCombo)
	{
	case 1:
		lookVector.x() *= 150.f;
		lookVector.y() *= 150.f;
		lookVector.z() *= 150.f;
		MovementComponent->AddImpulse(lookVector, 1.f);


		break;
	case 2:
		lookVector.x() *= 150.f;
		lookVector.y() *= 150.f;
		lookVector.z() *= 150.f;
		MovementComponent->AddImpulse(lookVector, 1.f);
		break;

	case 3:
		lookVector.x() *= 150.f;
		lookVector.y() *= 150.f;
		lookVector.z() *= 150.f;
		MovementComponent->AddImpulse(lookVector, 1.f);
		break;

	case 4:
		lookVector.x() *= 150.f;
		lookVector.y() *= 150.f;
		lookVector.z() *= 150.f;
		MovementComponent->AddImpulse(lookVector, 1.f);
		break;
	}
}

void Character::AttackEndComboState()
{
	IsComboInputOn = false;
	CanNextCombo = false;
	CurrentCombo = 0;
}
}
