﻿#pragma once

#include "EngineMinimal.h"
#include "Protocol/Types.h"

UENUM(BlueprintType)
enum class UItemType : uint8
{
	Weapon = static_cast<int>(TheShift::ItemType::Weapon),
	Armor = static_cast<int>(TheShift::ItemType::Armor),
};

UENUM(BlueprintType)
enum class UWeaponItemType : uint8
{
	GreatSword = static_cast<int>(TheShift::WeaponItemType::GreatSword),
	Bow = static_cast<int>(TheShift::WeaponItemType::Box),
};

UENUM(BlueprintType)
enum class UArmorItemType : uint8
{
	None = static_cast<int>(TheShift::ArmorItemType::None),
	HeavyArmor = static_cast<int>(TheShift::ArmorItemType::HeavyArmor),
	LightArmor = static_cast<int>(TheShift::ArmorItemType::LightArmor),
};
