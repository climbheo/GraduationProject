﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Protocol/Json_Formatter/JsonWriter.h"
#include "Protocol/Types.h"

#include "Engine/World.h"
#include "Misc/Paths.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TSMapInfoExporter.generated.h"

using namespace TheShift;

UCLASS()
class THESHIFT_CLIENT_API ATSMapInfoExporter : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATSMapInfoExporter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	JsonFormatter::JsonWriter JsonExporter;
	void ExportMapInfo();
};
