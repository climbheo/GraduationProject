// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "EngineMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Protocol/Types.h"
#include "Animation/AnimInstance.h"
#include "AnimNotifiesExporter.generated.h"


/**
 * 
 */
UCLASS()
class THESHIFT_CLIENT_API UAnimNotifiesExporter : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	static void ExportAnimMontageNotifies(std::vector<TheShift::AnimNotifyInfo>* pOut, UAnimMontage* pMontage, std::wstring AdditionalName = L"");
	static void ExportSlotAnimNotifies(std::vector<TheShift::AnimNotifyInfo>* pOut, UAnimSequence* pSlotAnim, float PlayRate = 1.f);
};
