#pragma once

#include "Vendor/include/rapidjson/document.h"
#include "Vendor/include/rapidjson/istreamwrapper.h"
#include "Vendor/include/rapidjson/reader.h"
#include <fstream>
#include <vector>
#include <set>
#include "Protocol/Types.h"

class FString;

namespace TheShift
{
	namespace JsonFormatter
	{
		class JsonReader
		{
		public:
			JsonReader();
			JsonReader(const char* path);

			bool Open(const char* path);
			bool Close();

	bool ImportActorInfo(const char* path, std::vector<ActorInfo>& info);
	// This function is for Client. For some reason, fstream and document don't work as intended.
	bool ImportActorInfoFromString(const char* str, std::vector<ActorInfo>& info);
	bool ImportMapInfo(const char* path, MapInfo& info);
	bool ImportMapInfoFromString(const char* str, MapInfo& info);
	bool ImportAnimNotifyInfo(const char* path, std::set<AnimNotifyInfo>& info);

		private:
			std::ifstream Ifs;
			rapidjson::IStreamWrapper Isw;
			rapidjson::Document Doc;

			bool ReadActorInfo(std::vector<ActorInfo>& info);
			bool ReadMapInfo(MapInfo& info);
			bool ReadAnimNotifyInfo(std::set<AnimNotifyInfo>& info);
			rapidjson::ParseErrorCode ParseFile(const char* path);
			rapidjson::ParseErrorCode ParseString(const char* str);
		};
	}
}
