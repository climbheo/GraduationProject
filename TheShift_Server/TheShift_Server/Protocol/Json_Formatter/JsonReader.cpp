﻿#include "JsonReader.h"
#include "Vendor/include/rapidjson/stream.h"
#include "Vendor/include/rapidjson/encodedstream.h"
#include "Protocol/Protocol.h"

#ifdef TSCLIENT
#include "Containers/UnrealString.h"
#endif

namespace TheShift
{
namespace JsonFormatter
{
JsonReader::JsonReader(const char* path) : Isw(Ifs)
{
	Open(path);
}

JsonReader::JsonReader() : Isw(Ifs)
{

}

bool JsonReader::Open(const char* path)
{
	if (Ifs.is_open())
	{
		return true;
	}
	Ifs.open(path, std::fstream::in);

	if (Ifs.is_open())
	{
		Doc.SetObject();
		return true;
	}
	else
	{
		return false;
	}
}

bool JsonReader::Close()
{
	if (!Ifs.is_open())
	{
		return true;
	}
	Ifs.close();

	if (Ifs.is_open())
	{
		return false;
	}
	else
	{
		return true;
	}
}

bool JsonReader::ImportActorInfo(const char* path, std::vector<ActorInfo>& info)
{
	if (Open(path))
	{
		if (ParseFile(path) == rapidjson::ParseErrorCode::kParseErrorNone)
		{
			if (ReadActorInfo(info))
			{
				Close();
				return true;
			}
		}
	}

	return false;
}

bool JsonReader::ImportActorInfoFromString(const char* str, std::vector<ActorInfo>& info)
{
	if (ParseString(str) == rapidjson::ParseErrorCode::kParseErrorNone)
	{
		if (ReadActorInfo(info))
		{
			Close();
			return true;
		}
	}
	return false;
}

bool JsonReader::ImportMapInfoFromString(const char* str, MapInfo& info)
{
	if (ParseString(str) == rapidjson::ParseErrorCode::kParseErrorNone)
	{
		if (ReadMapInfo(info))
		{
			Close();
			return true;
		}
	}

	return false;
}

bool JsonReader::ImportMapInfo(const char* path, MapInfo& info)
{
	if (Open(path))
	{
		if (ParseFile(path) == rapidjson::ParseErrorCode::kParseErrorNone)
		{
			if (ReadMapInfo(info))
			{
				Close();
				return true;
			}
		}
	}

	return false;
}

bool JsonReader::ImportAnimNotifyInfo(const char* path, std::set<AnimNotifyInfo>& info)
{
	if (Open(path))
	{
		if (ParseFile(path) == rapidjson::ParseErrorCode::kParseErrorNone)
		{
			if (ReadAnimNotifyInfo(info))
			{
				Close();
				return true;
			}
		}
	}
	return false;
}
bool JsonReader::ReadActorInfo(std::vector<ActorInfo>& info)
{
	if (Doc.HasParseError() || !Doc.HasMember("Actor Info"))
		return false;

	assert(!Doc.HasParseError());
	assert(Doc.HasMember("Actor Info"));
	const auto& ActorInfoArray = Doc["Actor Info"];
	assert(ActorInfoArray.IsArray());

	if (ActorInfoArray.Size() == 0)
		return false;

	for (rapidjson::SizeType i = 0; i < ActorInfoArray.Size(); ++i)
	{
		// Read ActorInfo from json file
		const auto& Element = ActorInfoArray[i].GetObject();

		ActorInfo Info;
		// Type
		assert(Element.HasMember("Actor Type"));
		Info.Type = static_cast<ActorType>(Element["Actor Type"].GetUint());

		//////////// Stat ////////////
		assert(Element.HasMember("Stat"));
		const auto& Stat = Element["Stat"].GetObject();

		// Mass
		assert(Stat.HasMember("Mass"));
		Info.Stat.Mass = Stat["Mass"].GetFloat();

		// Run Speed
		assert(Stat.HasMember("Forward Run Speed"));
		Info.Stat.ForwardRunSpeed = Stat["Forward Run Speed"].GetFloat();
		assert(Stat.HasMember("Side Run Speed"));
		Info.Stat.SideRunSpeed = Stat["Side Run Speed"].GetFloat();
		assert(Stat.HasMember("Back Run Speed"));
		Info.Stat.BackRunSpeed = Stat["Back Run Speed"].GetFloat();

		// Walk Speed
		assert(Stat.HasMember("Forward Walk Speed"));
		Info.Stat.ForwardWalkSpeed = Stat["Forward Walk Speed"].GetFloat();
		assert(Stat.HasMember("Side Walk Speed"));
		Info.Stat.SideWalkSpeed = Stat["Side Walk Speed"].GetFloat();
		assert(Stat.HasMember("Back Walk Speed"));
		Info.Stat.BackWalkSpeed = Stat["Back Walk Speed"].GetFloat();

		// Run Acceleration
		assert(Stat.HasMember("Forward Run Acceleration"));
		Info.Stat.ForwardRunAcceleration = Stat["Forward Run Acceleration"].GetFloat();
		assert(Stat.HasMember("Side Run Acceleration"));
		Info.Stat.SideRunAcceleration = Stat["Side Run Acceleration"].GetFloat();
		assert(Stat.HasMember("Back Run Acceleration"));
		Info.Stat.BackRunAcceleration = Stat["Back Run Acceleration"].GetFloat();

		// Walk Acceleration
		assert(Stat.HasMember("Forward Walk Acceleration"));
		Info.Stat.ForwardWalkAcceleration = Stat["Forward Walk Acceleration"].GetFloat();
		assert(Stat.HasMember("Side Walk Acceleration"));
		Info.Stat.SideWalkAcceleration = Stat["Side Walk Acceleration"].GetFloat();
		assert(Stat.HasMember("Back Walk Acceleration"));
		Info.Stat.BackWalkAcceleration = Stat["Back Walk Acceleration"].GetFloat();
		//////////////////////////////



		// Add to array
		info.emplace_back(Info);
	}

	return true;
}

bool JsonReader::ReadMapInfo(MapInfo& info)
{
	if (Doc.HasParseError() || !Doc.HasMember("Map Info"))
		return false;

	const auto& MapInfoValue = Doc["Map Info"].GetObject();

	assert(MapInfoValue.HasMember("Map Type"));
	info.Type = static_cast<MapType>(MapInfoValue["Map Type"].GetUint());

	assert(MapInfoValue.HasMember("Heightmaps"));
	const auto& Heightmaps = MapInfoValue["Heightmaps"];
	assert(Heightmaps.IsArray());


	for (rapidjson::SizeType i = 0; i < Heightmaps.Size(); ++i)
	{
		HeightmapInfo HeightmapValue;
		const auto& Element = Heightmaps[i];
		assert(Element.HasMember("Name"));
		HeightmapValue.FileName = Element["Name"].GetString();
		assert(Element.HasMember("Position"));
		const auto& PositionValue = Element["Position"].GetObject();
		assert(PositionValue.HasMember("X"));
		HeightmapValue.Position.X = PositionValue["X"].GetFloat();
		assert(PositionValue.HasMember("Y"));
		HeightmapValue.Position.Y = PositionValue["Y"].GetFloat();
		assert(PositionValue.HasMember("Z"));
		HeightmapValue.Position.Z = PositionValue["Z"].GetFloat();

		assert(Element.HasMember("Rotation"));
		const auto& RotationValue = Element["Rotation"].GetObject();
		assert(RotationValue.HasMember("X"));
		HeightmapValue.Rotation.X = RotationValue["X"].GetFloat();
		assert(RotationValue.HasMember("Y"));
		HeightmapValue.Rotation.Y = RotationValue["Y"].GetFloat();
		assert(RotationValue.HasMember("Z"));
		HeightmapValue.Rotation.Z = RotationValue["Z"].GetFloat();

		assert(Element.HasMember("Scale"));
		const auto& ScaleValue = Element["Scale"].GetObject();
		assert(ScaleValue.HasMember("X"));
		HeightmapValue.Scale.X = ScaleValue["X"].GetFloat();
		assert(ScaleValue.HasMember("Y"));
		HeightmapValue.Scale.Y = ScaleValue["Y"].GetFloat();
		assert(ScaleValue.HasMember("Z"));
		HeightmapValue.Scale.Z = ScaleValue["Z"].GetFloat();

		info.Heightmaps.emplace_back(HeightmapValue);
	}

	assert(MapInfoValue.HasMember("Actor Placement Info"));
	const auto& ActorPlacementInfoArray = MapInfoValue["Actor Placement Info"];
	assert(ActorPlacementInfoArray.IsArray());

	if (ActorPlacementInfoArray.Size() == 0)
		return false;

	for (rapidjson::SizeType i = 0; i < ActorPlacementInfoArray.Size(); ++i)
	{
		// Read Map info from json
		const auto& Element = ActorPlacementInfoArray[i].GetObject();
		ActorPlacementInfo ActorInfo;

		assert(Element.HasMember("Tag"));
		ActorInfo.Tag = Element["Tag"].GetString();

		assert(Element.HasMember("Actor Type"));
		ActorInfo.Type = static_cast<ActorType>(Element["Actor Type"].GetUint());

		assert(Element.HasMember("Position"));
		const auto& PositionVectorValue = Element["Position"].GetObject();

		assert(PositionVectorValue.HasMember("X") && PositionVectorValue.HasMember("Y") && PositionVectorValue.HasMember("Z"));
		ActorInfo.ActorPosition.X = PositionVectorValue["X"].GetFloat();
		ActorInfo.ActorPosition.Y = PositionVectorValue["Y"].GetFloat();
		ActorInfo.ActorPosition.Z = PositionVectorValue["Z"].GetFloat();

		assert(Element.HasMember("Rotation"));
		const auto& RotationVectorValue = Element["Rotation"].GetObject();

		assert(RotationVectorValue.HasMember("X") && RotationVectorValue.HasMember("Y") && RotationVectorValue.HasMember("Z"));
		ActorInfo.ActorRotation.X = RotationVectorValue["X"].GetFloat();
		ActorInfo.ActorRotation.Y = RotationVectorValue["Y"].GetFloat();
		ActorInfo.ActorRotation.Z = RotationVectorValue["Z"].GetFloat();

		//////////// Bounding Volume Info ////////////
		assert(Element.HasMember("Bound Info"));
		const auto& BoundInfo = Element["Bound Info"];
		assert(BoundInfo.IsArray());
		for (rapidjson::SizeType j = 0; j < BoundInfo.Size(); ++j)
		{
			BoundingVolumeInfo VolumeInfo;
			auto VolumeValue = BoundInfo[j].GetObject();
			assert(VolumeValue.HasMember("Bounding Volume Type"));
			VolumeInfo.BoundingVolumeType_ = static_cast<BoundingVolumeType>(VolumeValue["Bounding Volume Type"].GetUint());

			// Box에 필요한 Extent
			if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::AABB ||
				VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::OBB)
			{
				assert(VolumeValue.HasMember("Scaled Extent"));
				const auto& ExtentVectorValue = VolumeValue["Scaled Extent"].GetObject();

				assert(ExtentVectorValue.HasMember("X") && ExtentVectorValue.HasMember("Y") && ExtentVectorValue.HasMember("Z"));
				VolumeInfo.ScaledExtent.X = ExtentVectorValue["X"].GetFloat();
				VolumeInfo.ScaledExtent.Y = ExtentVectorValue["Y"].GetFloat();
				VolumeInfo.ScaledExtent.Z = ExtentVectorValue["Z"].GetFloat();
			}

			// Capsule에 필요한 Half height, Radius
			else if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::Capsule)
			{
				assert(VolumeValue.HasMember("Half Height"));
				VolumeInfo.HalfHeight = VolumeValue["Half Height"].GetFloat();

				assert(VolumeValue.HasMember("Radius"));
				VolumeInfo.Radius = VolumeValue["Radius"].GetFloat();
			}

			// Sphere 에 필요한 Radius
			else if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::Sphere)
			{
				assert(VolumeValue.HasMember("Radius"));
				VolumeInfo.Radius = VolumeValue["Radius"].GetFloat();
			}

			assert(VolumeValue.HasMember("Relative Location"));
			const auto& OriginVectorValue = VolumeValue["Relative Location"].GetObject();

			assert(OriginVectorValue.HasMember("X") && OriginVectorValue.HasMember("Y") && OriginVectorValue.HasMember("Z"));
			VolumeInfo.RelativeLocation.X = OriginVectorValue["X"].GetFloat();
			VolumeInfo.RelativeLocation.Y = OriginVectorValue["Y"].GetFloat();
			VolumeInfo.RelativeLocation.Z = OriginVectorValue["Z"].GetFloat();

			assert(VolumeValue.HasMember("Relative Rotation"));
			const auto& RotationOffsetVectorValue = VolumeValue["Relative Rotation"].GetObject();

			assert(RotationOffsetVectorValue.HasMember("X") && RotationOffsetVectorValue.HasMember("Y") && RotationOffsetVectorValue.HasMember("Z"));
			VolumeInfo.RelativeRotation.X = RotationOffsetVectorValue["X"].GetFloat();
			VolumeInfo.RelativeRotation.Y = RotationOffsetVectorValue["Y"].GetFloat();
			VolumeInfo.RelativeRotation.Z = RotationOffsetVectorValue["Z"].GetFloat();

			ActorInfo.BoundInfo.emplace_back(VolumeInfo);
		}
		//////////////////////////////////////////////

		////////////// Inventory Info ////////////////
		if(Element.HasMember("Inventory Info"))
		{
			const auto& InventoryInfo = Element["Inventory Info"];
			assert(InventoryInfo.IsArray());

			for (rapidjson::SizeType j = 0; j < InventoryInfo.Size(); ++j)
			{
				ItemInfo Item;
				auto ItemValue = InventoryInfo[j].GetObject();
				assert(ItemValue.HasMember("Index"));
				Item.Index = ItemValue["Index"].GetInt();
				assert(ItemValue.HasMember("Category"));
				Item.ItemCategory = ItemValue["Category"].GetInt();
				assert(ItemValue.HasMember("Type"));
				Item.ItemType = ItemValue["Type"].GetInt();

				ActorInfo.InventoryInfo.emplace_back(Item);
			}
		}
		//////////////////////////////////////////////		

		assert(Element.HasMember("Is Movable"));
		const auto& IsMovable = Element["Is Movable"].GetBool();
		ActorInfo.IsMovable = IsMovable;

		info.ActorPlacements.emplace_back(ActorInfo);
	}

	assert(MapInfoValue.HasMember("Actor Type Info"));
	const auto& ActorTypeInfoArray = MapInfoValue["Actor Type Info"];
	assert(ActorTypeInfoArray.IsArray());

	for (rapidjson::SizeType i = 0; i < ActorTypeInfoArray.Size(); ++i)
	{
		ActorInfo ActorTypeInfo;

		const auto& Element = ActorTypeInfoArray[i].GetObject();

		assert(Element.HasMember("Actor Type"));
		ActorTypeInfo.Type = static_cast<ActorType>(Element["Actor Type"].GetInt());

		//////////// Stat ////////////
		assert(Element.HasMember("Stat"));
		const auto& Stat = Element["Stat"].GetObject();

		// Mass
		assert(Stat.HasMember("Mass"));
		ActorTypeInfo.Stat.Mass = Stat["Mass"].GetFloat();

		// Run Speed
		assert(Stat.HasMember("Forward Run Speed"));
		ActorTypeInfo.Stat.ForwardRunSpeed = Stat["Forward Run Speed"].GetFloat();
		assert(Stat.HasMember("Side Run Speed"));
		ActorTypeInfo.Stat.SideRunSpeed = Stat["Side Run Speed"].GetFloat();
		assert(Stat.HasMember("Back Run Speed"));
		ActorTypeInfo.Stat.BackRunSpeed = Stat["Back Run Speed"].GetFloat();

		// Walk Speed
		assert(Stat.HasMember("Forward Walk Speed"));
		ActorTypeInfo.Stat.ForwardWalkSpeed = Stat["Forward Walk Speed"].GetFloat();
		assert(Stat.HasMember("Side Walk Speed"));
		ActorTypeInfo.Stat.SideWalkSpeed = Stat["Side Walk Speed"].GetFloat();
		assert(Stat.HasMember("Back Walk Speed"));
		ActorTypeInfo.Stat.BackWalkSpeed = Stat["Back Walk Speed"].GetFloat();

		// Run Acceleration
		assert(Stat.HasMember("Forward Run Acceleration"));
		ActorTypeInfo.Stat.ForwardRunAcceleration = Stat["Forward Run Acceleration"].GetFloat();
		assert(Stat.HasMember("Side Run Acceleration"));
		ActorTypeInfo.Stat.SideRunAcceleration = Stat["Side Run Acceleration"].GetFloat();
		assert(Stat.HasMember("Back Run Acceleration"));
		ActorTypeInfo.Stat.BackRunAcceleration = Stat["Back Run Acceleration"].GetFloat();

		// Walk Acceleration
		assert(Stat.HasMember("Forward Walk Acceleration"));
		ActorTypeInfo.Stat.ForwardWalkAcceleration = Stat["Forward Walk Acceleration"].GetFloat();
		assert(Stat.HasMember("Side Walk Acceleration"));
		ActorTypeInfo.Stat.SideWalkAcceleration = Stat["Side Walk Acceleration"].GetFloat();
		assert(Stat.HasMember("Back Walk Acceleration"));
		ActorTypeInfo.Stat.BackWalkAcceleration = Stat["Back Walk Acceleration"].GetFloat();
		//////////////////////////////

		//////////// Bounding Volume Info ////////////
		assert(Element.HasMember("Bound Info"));
		const auto& BoundInfo = Element["Bound Info"];
		assert(BoundInfo.IsArray());
		for (rapidjson::SizeType j = 0; j < BoundInfo.Size(); ++j)
		{
			BoundingVolumeInfo VolumeInfo;
			auto VolumeValue = BoundInfo[j].GetObject();
			assert(VolumeValue.HasMember("Bounding Volume Type"));
			VolumeInfo.BoundingVolumeType_ = static_cast<BoundingVolumeType>(VolumeValue["Bounding Volume Type"].GetUint());

			// Box에 필요한 Extent
			if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::AABB ||
				VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::OBB)
			{
				assert(VolumeValue.HasMember("Scaled Extent"));
				const auto& ExtentVectorValue = VolumeValue["Scaled Extent"].GetObject();

				assert(ExtentVectorValue.HasMember("X") && ExtentVectorValue.HasMember("Y") && ExtentVectorValue.HasMember("Z"));
				VolumeInfo.ScaledExtent.X = ExtentVectorValue["X"].GetFloat();
				VolumeInfo.ScaledExtent.Y = ExtentVectorValue["Y"].GetFloat();
				VolumeInfo.ScaledExtent.Z = ExtentVectorValue["Z"].GetFloat();
			}

			// Capsule에 필요한 Half height, Radius
			else if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::Capsule)
			{
				assert(VolumeValue.HasMember("Half Height"));
				VolumeInfo.HalfHeight = VolumeValue["Half Height"].GetFloat();

				assert(VolumeValue.HasMember("Radius"));
				VolumeInfo.Radius = VolumeValue["Radius"].GetFloat();
			}

			// Sphere 에 필요한 Radius
			else if (VolumeInfo.BoundingVolumeType_ == BoundingVolumeType::Sphere)
			{
				assert(VolumeValue.HasMember("Radius"));
				VolumeInfo.Radius = VolumeValue["Radius"].GetFloat();
			}

			assert(VolumeValue.HasMember("Relative Location"));
			const auto& OriginVectorValue = VolumeValue["Relative Location"].GetObject();

			assert(OriginVectorValue.HasMember("X") && OriginVectorValue.HasMember("Y") && OriginVectorValue.HasMember("Z"));
			VolumeInfo.RelativeLocation.X = OriginVectorValue["X"].GetFloat();
			VolumeInfo.RelativeLocation.Y = OriginVectorValue["Y"].GetFloat();
			VolumeInfo.RelativeLocation.Z = OriginVectorValue["Z"].GetFloat();

			assert(VolumeValue.HasMember("Relative Rotation"));
			const auto& RotationOffsetVectorValue = VolumeValue["Relative Rotation"].GetObject();

			assert(RotationOffsetVectorValue.HasMember("X") && RotationOffsetVectorValue.HasMember("Y") && RotationOffsetVectorValue.HasMember("Z"));
			VolumeInfo.RelativeRotation.X = RotationOffsetVectorValue["X"].GetFloat();
			VolumeInfo.RelativeRotation.Y = RotationOffsetVectorValue["Y"].GetFloat();
			VolumeInfo.RelativeRotation.Z = RotationOffsetVectorValue["Z"].GetFloat();

			ActorTypeInfo.BoundInfo.emplace_back(VolumeInfo);
		}
		//////////////////////////////////////////////

		//////////// Is Movable ////////////
		assert(Element.HasMember("Is Movable"));
		ActorTypeInfo.IsMovable = Element["Is Movable"].GetBool();
		////////////////////////////////////

		info.ActorTypeInfo.emplace_back(ActorTypeInfo);
	}

	return true;
}

bool JsonReader::ReadAnimNotifyInfo(std::set<AnimNotifyInfo>& info)
{
	if (Doc.HasParseError() || !Doc.HasMember("AnimNotify Info"))
		return false;

	assert(!Doc.HasParseError());
	assert(Doc.HasMember("AnimNotify Info"));
	const auto& AnimNotifyInfoArray = Doc["AnimNotify Info"];
	assert(AnimNotifyInfoArray.IsArray());

	if (AnimNotifyInfoArray.Size() == 0)
		return false;

	for (rapidjson::SizeType i = 0; i < AnimNotifyInfoArray.Size(); ++i)
	{
		// Read AnimNotifyInfo from json file
		const auto& Element = AnimNotifyInfoArray[i].GetObject();

		AnimNotifyInfo Info;
		// AnimName
		assert(Element.HasMember("Anim Name"));
		Info.AnimName = Element["Anim Name"].GetString();

		//////////// Notify ////////////
		assert(Element.HasMember("Notifies"));
		const auto& Notifies = Element["Notifies"];
		assert(Notifies.IsArray());

		for (rapidjson::SizeType j = 0; j < Notifies.Size(); ++j)
		{
			//NotifyName
			assert(Notifies[j].HasMember("Notify Name"));
			std::string NotifyName = Notifies[j]["Notify Name"].GetString();

			// Mass
			assert(Notifies[j].HasMember("Trigger Time"));
			float TriggerTime = Notifies[j]["Trigger Time"].GetFloat();

			Info.AnimNotifies.emplace_back(NotifyName, TriggerTime);
		}
		//////////////////////////////

		// Add to array
		info.emplace(Info);
	}
	return true;
}

rapidjson::ParseErrorCode JsonReader::ParseFile(const char* path)
{
	assert(Doc.IsObject());
	Doc.ParseStream(Isw);
	return Doc.GetParseError();
}

rapidjson::ParseErrorCode JsonReader::ParseString(const char* str)
{
	assert(Doc.IsObject());
	rapidjson::StringStream InputStream(str);
	Doc.ParseStream<rapidjson::ParseFlag::kParseStopWhenDoneFlag>(InputStream);
	return Doc.GetParseError();
}
}
}
