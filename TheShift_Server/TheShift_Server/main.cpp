﻿#include "server.h"
#include "Protocol/Log.h"
#include <locale>

using namespace std::chrono_literals;

int main()
{
	spdlog::set_level(spdlog::level::debug);
	TheShiftServer.Run();


	////////////////
	// Timer test //
	////////////////

 //   GameState State;
 //   std::chrono::high_resolution_clock::time_point Last = std::chrono::high_resolution_clock::now();

	//// SetTimer 에 chrono_literal (ms, s, min)을 전달하면 해당 시간만큼 지난 후 자동으로 불린다
	//// Register 로 함수 등록
	//auto NewActivator1 = std::make_shared<Activator>();
	//NewActivator1->Register([]() {spdlog::info("Activated1, 1 sec"); });
	//NewActivator1->SetTimer(1000ms);

	//// 생성자에 함수 전달
	//auto NewActivator2 = std::make_shared<Activator>([]() {spdlog::info("Activated2, 3 sec"); });
	//NewActivator2->SetTimer(3s);

	//// Register 로 bool 변수 등록
	//bool TrueIfCoolDownGetsCalled1 = false;
	//auto NewCoolDown1 = std::make_shared<CoolDown>();
	//NewCoolDown1->Register(&TrueIfCoolDownGetsCalled1);
	//NewCoolDown1->SetTimer(1min);

	//// 생성자에 bool pointer 전달
	//bool TrueIfCoolDownGetsCalled2 = false;
	//auto NewCoolDown2 = std::make_shared<CoolDown>(&TrueIfCoolDownGetsCalled2);
	//NewCoolDown2->SetTimer(1500ms);


	//State.AddToTimer(NewActivator1);
	//State.AddToTimer(NewActivator2);
	//State.AddToTimer(NewCoolDown1);
	//State.AddToTimer(NewCoolDown2);

 //   while(true)
	//{
	//	auto Now = std::chrono::high_resolution_clock::now();
	//	State.Update(std::chrono::duration_cast<std::chrono::nanoseconds>(Now - Last));
	//	Last = Now;
	//}


	////////////////////
	// Transform Test //
	////////////////////

	//Transform TestTransform1;
	//TestTransform1.RotateWithDegrees(30.f, -50.f, 40.f);
	//auto Degrees1 = TestTransform1.GetWorldDegrees();
	//auto Quat1 = TestTransform1.GetWorldRotation();

	//Transform TestTransform2;
	//TestTransform2.RotateWithDegrees(30.f, -30.f, 0.f);
	//auto Degrees2 = TestTransform2.GetWorldDegrees();
	//auto Quat2 = TestTransform2.GetWorldRotation();

	//Transform TestTransform3;
	//TestTransform3.SetRotationWithDegrees(90.f, 30.f, -40.f);
	//TestTransform3.Translate(0.f, 0.f, 1.f);
	//Eigen::Vector3f Degrees3 = TestTransform3.GetWorldDegrees();
	//Eigen::Vector3f Location = TestTransform3.GetWorldTranslation();
	//Eigen::Quaternionf Quat = TestTransform3.GetWorldRotation();

	//Transform TestTransform33;
	//TestTransform33.RotateWithDegrees(90.f, 30.f, -40.f);
	//TestTransform33.Translate(0.f, 0.f, 1.f);
	//Eigen::Vector3f Degrees33 = TestTransform33.GetWorldDegrees();
	//Eigen::Vector3f Location33 = TestTransform33.GetWorldTranslation();
	//Eigen::Quaternionf Quat33 = TestTransform33.GetWorldRotation();

	//Transform TestTransform333;
	//TestTransform333.RotateWithDegrees(Degrees33);
	//TestTransform333.Translate(0.f, 0.f, 1.f);
	//Eigen::Vector3f Degrees333 = TestTransform333.GetWorldDegrees();
	//Eigen::Vector3f Location333 = TestTransform333.GetWorldTranslation();
	//Eigen::Quaternionf Quat333 = TestTransform333.GetWorldRotation();

	//Transform TestTransform4;
	//TestTransform4.SetRotation(Quat);
	//TestTransform4.Translate(0.f, 0.f, 1.f);
	//Eigen::Vector3f Degrees4 = TestTransform4.GetWorldDegrees();
	//Eigen::Vector3f Location4 = TestTransform4.GetWorldTranslation();
	//Eigen::Quaternionf Quat4 = TestTransform4.GetWorldRotation();
}
