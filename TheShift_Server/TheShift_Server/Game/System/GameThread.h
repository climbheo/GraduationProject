﻿#pragma once

#include "common.h"
#include "Game/System/MemoryAllocators.h"
#include "Protocol/MessageBuilder.h"
#include "Protocol/Protocol.h"
#include "Protocol/Serializables.h"
#include "Session.h"
#include "Game/System/GameState.h"

#include <chrono>
#include <atomic>
#include <map>
#include "Protocol/atomic_memory_pool.h"
#include "tbb/include/tbb/concurrent_queue.h"

using namespace TheShift;
using namespace std::chrono_literals;

class GameThread
{
public:
	GameThread();
	GameThread(int8_t ThreadIndex);
	~GameThread();

	void Join();
	bool IsReady();
	void SetMessageQueue(tbb::concurrent_queue<Message*>* newQueue);
	//void SetMessagePool(MemoryPool<Message>* messagePool);
	//void SetSendRequirements(tbb::concurrent_queue<SocketInfo*>* sendQueue, MemoryPool<SocketInfo>* socketInfoPool,
	//                         HANDLE* sendEvent);
	void SetUpdateTickrate(int tickrate);
	void SetNetworkUpdateTickrate(int tickrate);

	// Thread 임시 멈춤
	void Suspend();

	// Thread 재시작
	void Resume();

	// 모든 메모리 해제
	void Clear();

	bool CheckSuspended();

	void Run();

	int GetSessionCount() const;

	// Server에서 부여한 Id로 Session을 할당한다.
	void CreateSession(SessionId id);

	void AddPlayerToSession(std::shared_ptr<Avatar> client, SessionId sessionId);

	void StartSession(SessionId sessionId, MapType map);
	void WakeThread();


private:
	std::thread Thread;


	// Session의 본체는 GameThread가 가진다.
	// Server에서 GameThread로 Message를 보내 통신
	std::map<SessionId, std::shared_ptr<Session>> Sessions;


	// Queuer	: Receiver
	// Dequeuer	: GameThread
	//MessageQueue<Message>* MsgQueue;
	tbb::concurrent_queue<Message*>* MsgQueue = nullptr;
	//tbb::concurrent_queue<SocketInfo*>* SendQueue = nullptr;
	//MemoryPool<Message>* MessagePool = nullptr;
	//MemoryPool<SocketInfo>* SocketInfoPool = nullptr;
	//HANDLE* SendEvent = nullptr;

	// 밖에서 이 thread에 할당된 메모리에 접글할 때 사용됨.
	ThreadIndex Index = INVALID_THREADINDEX;

	std::atomic<bool> SuspendReq = false;
	std::atomic<bool> IsSuspended = false;
	std::atomic<bool> IsRunning = false;
	std::atomic<bool> ForcedSuspend = false;

	// 게임의 로직
	void GameLoop();
	// 서버 내에서 게임이 업데이트 되는 interval
	std::chrono::nanoseconds UpdateTimestep;
	// 게임 state를 client들에게 보내는 interval
	std::chrono::nanoseconds NetworkUpdateTimestep;
	int Tickrate;

	void InternalSuspend();
	void InternalResume();

};

