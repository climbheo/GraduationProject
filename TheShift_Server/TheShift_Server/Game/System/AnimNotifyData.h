﻿#pragma once

#include "Protocol/Types.h"
#include <map>
#include <set>

using namespace TheShift;

class AnimNotifyData
{
private:
	AnimNotifyData();
	~AnimNotifyData() {}

public:
	static AnimNotifyData& GetInstance()
	{
		if (!Instance_)
			Instance_ = new AnimNotifyData;
		return *Instance_;
	}

	static void DestroyInstance()
	{
		if (Instance_)
		{
			delete Instance_;
			Instance_ = nullptr;
		}
	}

public:
	const std::set<AnimNotifyInfo>* GetAnimNotifyInfo(std::string ActorName) const;

private:
	static AnimNotifyData* Instance_;
	//<객체 이름, 애님노티파이들의 Map->애님이름으로 접근 쉽게하려고 선택한 자료구조>
	//애님노티파이는 애님이름, 노티파이 배열로 구성되어있다.
	std::map <std::string, std::set<AnimNotifyInfo>> Data;
};

