﻿#pragma once

#include "common.h"
#include "Player.h"
#include <map>
#include <set>
#include <chrono>
#include "GameState.h"

// Lobby에서 확인할 수 있는 session의 정보를 저장한다.
struct SessionInfo
{
	SessionInfo();
	SessionInfo(SessionId newId);

	std::map<ClientId, std::shared_ptr<Avatar>> Players;
	SessionId Id;
	bool IsInGame = false;
	std::string Name;
	// Test용 기본 맵
	MapType Map = MapType::Landscape;
	ClientId HostId;
};

// Session 전체의 정보를 저장한다.
// 준비상태, 게임중 상태 모두를 저장한다.
// 예) Player정보, Actor목록, 정보.
class Session
{
public:
	Session();
	Session(SessionId NewId);

	bool UpdateGame(std::chrono::nanoseconds deltaTime);
	void ApplyMessage(Message* message);
	void AddNewPlayer(std::shared_ptr<Avatar> client);
	void StartGame(MapType map);
	void SetUpdateTimestep(float Timestep);
	std::map<ClientId, std::shared_ptr<Avatar>>* GetPlayers();
	std::map<UID, std::shared_ptr<Actor>>* GetActors();

	std::vector<std::shared_ptr<Serializables::SerializableData>>* GetDataToBeSent();
	void ClearDataToBeSent();

private:
	SessionId Id;
	GameState State;

};
