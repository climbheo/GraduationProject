﻿#pragma once

#include "common.h"
#include "Player.h"
#include "Session.h"
#include "Protocol/Serializables.h"

#include <map>

using namespace TheShift;

class Lobby
{
public:
	Lobby();

	bool IsPlayerInLobby(ClientId id) const;
	bool IsSessionInLobby(SessionId id) const;
	bool IsPlayerInSession(ClientId clientId, SessionId sessionId) const;
	bool IsEveryoneReady(SessionId id) const;
	bool IsInGame(SessionId id) const;

	bool Join(ClientId id);

	//TODO: Id뿐만 아니라 Avatar로 받아야 하는거 아닌가? 이름도 같이 저장해야하는데?
	SessionId SessionJoinOrCreate(ClientId clientId, SessionId sessionId);
	bool SessionExit(ClientId id);
	void DisconnectPlayer(ClientId id);
	bool PlayerReady(ClientId id);
	bool PlayerUnReady(ClientId id);
	void DestroySession(SessionId id);

	std::shared_ptr<Avatar> GetPlayerInfo(ClientId id);
	std::shared_ptr<SessionInfo> GetSessionInfo(SessionId sessionId);
	void GetPlayers(std::vector<std::shared_ptr<Avatar>>& players);
	void GetSessions(std::vector<std::shared_ptr<SessionInfo>>& sessions);
	void GetLobbyInfo(Serializables::LobbyInfo& info);
	void GetPlayersInLobby(std::vector<std::shared_ptr<Avatar>>& players);
	ClientId GetHostId(SessionId sessionId);

private:
	std::map<ClientId, std::shared_ptr<Avatar>> Players;
	std::map<SessionId, std::shared_ptr<SessionInfo>> Sessions;

	SessionId GetAvailableSessionId() const;
};

