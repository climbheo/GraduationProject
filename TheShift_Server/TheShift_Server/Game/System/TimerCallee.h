﻿#pragma once

#include <functional>
#include <chrono>


namespace TheShift
{
class TimerCallee
{
public:
	TimerCallee();
	virtual ~TimerCallee();
	virtual void operator()() = 0;
	virtual operator bool() const = 0;
	template<typename T>
	void SetTimer(T CountDown)
	{
		static_assert(std::is_same<std::chrono::minutes, T>::value ||
					  std::is_same<std::chrono::seconds, T>::value ||
					  std::is_same<std::chrono::milliseconds, T>::value,
					  "Only std::chrono::minutes, std::chrono::seconds, std::chrono::milliseconds can be passed.");

		auto Now = std::chrono::high_resolution_clock::now();
		TimePoint = Now + CountDown;

		TimeSetProperly = true;
	}

	std::chrono::high_resolution_clock::time_point TimePoint;
	bool TimeSetProperly;
};


/// <summary>
/// 특정 행동이 다시 실행될 수 있는 최소한의 시간인 Cool down을 자동으로 갱신하기 위한 객체
/// Priority Queue에 저장하여 지정한 시간이 지나면 생성자에서 전달한 bool& 변수를 true로 바꿔준다.
/// </summary>
class CoolDown : public TimerCallee
{
public:
	CoolDown();
	CoolDown(bool* trigger);
	virtual void operator()();
	virtual operator bool() const;
	void Register(bool* trigger);

private:
	bool* Trigger;
};

/// <summary>
/// 일정 시간 후 실행되어야 하는 함수가 있을때 사용한다.
/// Priority Queue에 저장하여 지정한 시간이 지나면 자동으로 함수를 실행하도록 사용한다.
/// </summary>
class Activator : public TimerCallee
{
public:
	Activator();
	Activator(std::function<void()> func);
	virtual void operator()();
	virtual operator bool() const;

	void Register(std::function<void()> func);

protected:
	std::function<void()> Behaviour;


};


/// <summary>
/// 기본적인 동작은 Activator와 동일하다.
/// 특정 상황에 액티베이터를 비활성화 해야할 필요가 있다면 쓰는 객체
/// </summary>

class CheckActivator : public Activator
{
public:
	CheckActivator() {}
	CheckActivator(std::function<void()> func) :Activator(func) {}
	virtual void operator()();
	virtual operator bool() const;

public:
	void SetActivated(bool isActivated);

private:
	bool isActivated = false;
};
}
