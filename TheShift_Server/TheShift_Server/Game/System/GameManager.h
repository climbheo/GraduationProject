﻿#pragma once

#include "common.h"
#include "GameThreadPool.h"
#include "Lobby.h"
#include "Game/System/Network/SendRequirements.h"
#include "Game/System/MemoryAllocators.h"


/// <summary>
/// Lobby를 포함한 모든 Game에 대한 처리를 한다.
/// </summary>
class GameManager
{
public:
	GameManager();
	void SetGameThreads(ThreadIndex numThreads);
	void Run();

	void ProcessPacketData(ClientId clientId, char* data, Serializables::Type dataType);
	void ClearClient(ClientId clientId);

	void ClearAll();

private:
	GameThreadPool GamePool;
	Lobby GameLobby;
	// Data 해석용 input stream
	InputMemoryStream InputStream;

	// Process packet functions
	void ProcessLobbyJoinReq(ClientId id, Serializables::LobbyJoinReq& data);
	void ProcessLobbyInfoReq(ClientId id, Serializables::LobbyInfoReq& data);
	void ProcessSessionJoinReq(ClientId id, Serializables::SessionJoinReq& data);
	void ProcessCreateAndStartSessionReq(ClientId id, Serializables::CreateAndStartSessionReq& data);

	void ProcessLobbyExit(ClientId id);
	void ProcessSessionExit(ClientId id, Serializables::SessionExit& data);

	// GamePool로 Message를 dispatch 할 경우 MessagePool에서 Message를 꺼내서 MessageQueue에 push한다.
	void ProcessChat(ClientId id, Serializables::Chat& data);
	void ProcessReady(ClientId id, Serializables::Ready& data);
	void ProcessMapSelect(ClientId id, Serializables::MapSelect& data);

	bool StartSession(SessionId id);

	// TODO: Game으로 가야하는 data는 모두 shared_ptr로 전달한다.
	template<typename T>
	typename std::enable_if<std::is_base_of<Serializables::SerializableData, T>::value>::type DispatchMessageToThread(
		ClientId id, std::shared_ptr<T>& data)
	{
		std::shared_ptr<Avatar> Player = GameLobby.GetPlayerInfo(id);
		if(Player != nullptr)
		{
			Message* NewMessage = MemoryAllocator::MessageAllocator.alloc();
			NewMessage->From = id;
			NewMessage->Dest = Player->SessionId_;
			NewMessage->Data = data;
			GamePool.DispatchMessageToThread(NewMessage);
		}
	}
	// void ProcessDisconnect(ClientId id, std::shared_ptr<Serializables::Disconnect>& data);
	// void ProcessLoadCompleted(ClientId id, std::shared_ptr<Serializables::LoadCompleted>& data);
	// void ProcessInput(ClientId id, std::shared_ptr<Serializables::Input>& data);
};


