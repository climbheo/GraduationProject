﻿#include "GameThread.h"

GameThread::GameThread()
{
	static_assert(true, "default constructor should not be called.");
}

GameThread::GameThread(int8_t ThreadIndex) : Index(ThreadIndex)
{
	SetUpdateTickrate(20);
	SetNetworkUpdateTickrate(20);
}

GameThread::~GameThread()
{
	//...메모리 해제
	Clear();

	if(Thread.joinable())
		Thread.join();

	spdlog::info("Thread {0} joined.", Index);
}

void GameThread::Join()
{
	Thread.join();
}

// TODO: 준비가 되었는지 확인.
bool GameThread::IsReady()
{
	return true;
}

void GameThread::SetMessageQueue(tbb::concurrent_queue<Message*>* NewQueue)
{
	MsgQueue = NewQueue;
}

/// <summary>
/// Tickrate에 기반해 Timestep을 계산한다.
/// </summary>
void GameThread::SetUpdateTickrate(int tickrate)
{
	Tickrate = tickrate;
	UpdateTimestep = std::chrono::nanoseconds(int(1000000000.f / float(tickrate)));
}

void GameThread::SetNetworkUpdateTickrate(int tickrate)
{
	NetworkUpdateTimestep = std::chrono::nanoseconds(int(1000000000.f / float(tickrate)));
}

void GameThread::Suspend()
{
	ForcedSuspend = true;
	SuspendReq = true;
}

void GameThread::Resume()
{
	ForcedSuspend = false;
	SuspendReq = false;
	ResumeThread(Thread.native_handle());
}

void GameThread::Clear()
{
	// TODO: 메모리 해제
}

bool GameThread::CheckSuspended()
{
	return IsSuspended;
}

// Thread를 생성하고 실행한다.
void GameThread::Run()
{
	// 생성 후 초기엔 Suspend. Session이 생성되면 그때 풀어주기
	SuspendReq = true;
	IsSuspended = false;
	IsRunning = true;

	Thread = std::thread(&GameThread::GameLoop, this);
	//spdlog::info("GameThread {0} created.", Index);
}

int GameThread::GetSessionCount() const
{
	return Sessions.size();
}

// TODO: Game logic구현
void GameThread::GameLoop()
{
	std::chrono::nanoseconds AccumulatedTime(0);
	std::chrono::nanoseconds NetworkUpdateTimer(0);
	std::chrono::nanoseconds Delta(0);
	std::chrono::high_resolution_clock::time_point Last = std::chrono::high_resolution_clock::now();


	//...Game logic

	while(IsRunning)
	{
		// Session이 없을때 suspend하기 위한 check용
		// Update시 session에 player가 없다면 Update에서 false를 return함
		// true를 단 한번도 return하지 않다면 suspend
		bool NoSession = true;

		// Suspend 요청이 있을 경우.
		if(SuspendReq == true)
		{
			SuspendReq = false;
			// Suspend되었다고 설정하고 스스로 잠자기.
			IsSuspended = true;
			SuspendThread(Thread.native_handle());
			IsSuspended = false;
			Last = std::chrono::high_resolution_clock::now();
		}

		std::chrono::high_resolution_clock::time_point Now = std::chrono::high_resolution_clock::now();
		Delta = std::chrono::duration_cast<std::chrono::nanoseconds>(Now - Last);
		Last = Now;
		AccumulatedTime += Delta;
		NetworkUpdateTimer += Delta;

		if(AccumulatedTime >= UpdateTimestep)
		{
			AccumulatedTime -= UpdateTimestep;
			// Update state
			// Pop message 하고 데이터 처리
			Message* NewMessage = nullptr;
			while(MsgQueue->try_pop(NewMessage))
			{
				// 각 Session에 message를 전달해준다.
				if(Sessions.find(NewMessage->Dest) != Sessions.end())
					Sessions.at(NewMessage->Dest)->ApplyMessage(NewMessage);


				// Message는 처리 후 해제해준다.
				MemoryAllocator::MessageAllocator.dealloc(NewMessage);
			}



			// Game state update
			for(auto Iter = Sessions.begin(); Iter != Sessions.end();)
			{
				if(Iter->second->UpdateGame(UpdateTimestep))
				{
					NoSession = false;
					++Iter;
				}
				else
				{
					Iter = Sessions.erase(Iter);
				}
			}

			// 이 Thread에 할당된 session이 없다면 Suspend시킨다.
			if(NoSession == true)
			{
				InternalSuspend();
				continue;
			}
		}

		if(NetworkUpdateTimer >= NetworkUpdateTimestep)
		{
			while(NetworkUpdateTimer >= NetworkUpdateTimestep)
				NetworkUpdateTimer -= NetworkUpdateTimestep;

			// 모든 Session을 순회하며
			for(auto& SessionPair : Sessions)
			{
				// TODO: 이 데이터들을 전부 묶어서 보내는게 더 효율적
				// TODO: 데이터 전달을 모두 base class로 전달하는데 derived class들의 함수가 안불리거나 하지 않나?
				// 해당 Session에 저장된 Update data들을
				for(auto& Data : *SessionPair.second->GetDataToBeSent())
				{
					// 해당 Session내의 Player들에게 보낸다.
					for(auto& PlayerPair : *SessionPair.second->GetPlayers())
					{
						Network::Send(PlayerPair.second->ClientId_, *Data);
					}
				}
				SessionPair.second->ClearDataToBeSent();
			}
			// Send state
		}
	}
}

/// <summary>
/// Session이 모두 해제된 Thread를 임시로 대기시킨다.
/// <!summary>
void GameThread::InternalSuspend()
{
	if(!ForcedSuspend)
	{
		SuspendReq = true;
	}
}

/// <summary>
/// 대기중인 Thread를 시작한다.
/// Suspend()로 강제로 Suspend된 상태는 깨울 수 없다.
/// Resume()으로 재개 가능
/// <!summary>
void GameThread::InternalResume()
{
	if(!ForcedSuspend)
	{
		SuspendReq = false;
		ResumeThread(Thread.native_handle());
	}
}

/// <summary>
/// 대기중인 Thread를 깨운다.
/// Suspend()로 강제로 Suspend된 상태는 깨울 수 없다.
/// Resume()으로 재개 가능
/// <!summary>
void GameThread::WakeThread()
{
	if(!ForcedSuspend)
	{
		SuspendReq = false;
		ResumeThread(Thread.native_handle());
	}
}

void GameThread::CreateSession(SessionId id)
{
	Sessions.insert(std::make_pair(id, std::make_shared<Session>()));
	//spdlog::info("Session created. SessionId: {0}", id);
	LOG_INFO("Session created. SessionId: %d", id);
}

void GameThread::AddPlayerToSession(std::shared_ptr<Avatar> client, SessionId sessionId)
{
	if(Sessions.find(sessionId) != Sessions.end())
	{
		Sessions[sessionId]->AddNewPlayer(client);
	}
}

/// <summary>
/// Session을 시작한다.
/// Player들에게 SessionStarted를 보내고
/// 모든 Player들로부터 Serializables::LoadCompleted 를 받으면
/// Serializables::GameStarted 를 보낸다.
/// </summary>
void GameThread::StartSession(SessionId sessionId, MapType map)
{
	if(Sessions.find(sessionId) != Sessions.end())
	{
		if(CheckSuspended())
		{
			InternalResume();
		}
		Sessions[sessionId]->SetUpdateTimestep(1.f / (float)Tickrate);
		Sessions[sessionId]->StartGame(map);
	}
}

