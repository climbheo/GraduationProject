﻿#include "Game/Actors/KnightMonster.h"
#include "Game/System/GameState.h"
#include "Game/Actors/Trigger.h"
#ifdef TSSERVER
#include "Game/System/TimerCallee.h"
#endif

using namespace std::chrono_literals;
using namespace std;
const std::set<AnimNotifyInfo>* KnightMonster::KnightMonsterAnimNotifies = nullptr;

namespace TheShift
{

KnightMonster::KnightMonster(UID Id) : Monster(Id)
{
	Type = ActorType::KnightMonster;
#ifdef TSSERVER
	IsHitAnimPlay = true;
	SetRecognizeRange(500.f);
	SetContactRange(150.f);

	//Get Pointer of Player AnimData
	if (!KnightMonsterAnimNotifies)
		KnightMonsterAnimNotifies = AnimNotifyData::GetInstance().GetAnimNotifyInfo("KnightMonster");
	else
		pMonsterAnimNotify = KnightMonsterAnimNotifies;
#endif
}
#ifdef TSSERVER

void KnightMonster::OnPlayerContact(std::shared_ptr<Actor> Player)
{
	// Cool-Down, CanAct check
	if(AttackCoolDown && CanAct && !GetHit())
	{
		AttackCoolDown = false;
		CanChase = false;
		std::shared_ptr<CoolDown> AttackCoolDownTimer = std::make_shared<CoolDown>();
		AttackCoolDownTimer->SetTimer(4s);
		// 기본 공격 Cool down 등록
		AttackCoolDownTimer->Register(&AttackCoolDown);
		// Timer에 등록
		CurrentGameState->AddToTimer(AttackCoolDownTimer);

		// 공격 함을 알림
		Serializables::ActWithTarget AttackMessage;
		AttackMessage.Self = Id;
		AttackMessage.TargetId = Player->GetId();

		if(PrevAttackNum == 1)
		{
			AttackMessage.Type = ActType::BaseAttack_1;
			PrevAttackNum = 2;
		}
		else
		{
			AttackMessage.Type = ActType::BaseAttack_2;
			PrevAttackNum = 1;
		}
		if (KnightMonsterAnimNotifies)
		{
			CurrentActivators.clear();

			//위의 animName에 해당하는 AnimNotifies를 찾고
			auto iter = KnightMonsterAnimNotifies->find(string("KnightMonster_Attack0") + to_string(static_cast<int>(AttackMessage.Type)));

			//모든 Notify를 실행한다.
			if (iter != KnightMonsterAnimNotifies->end())
			{
				auto& Notifies = iter->AnimNotifies;
				CurrentActivators.reserve(Notifies.size());
				for (auto& Notify : Notifies)
				{
					if (Notify.first.find("HitCheck") != string::npos)
					{
						shared_ptr<CheckActivator> HitActivator = make_shared<CheckActivator>();
						float hitCheckTime = Notify.second;

						HitActivator->SetTimer(chrono::milliseconds(long long(hitCheckTime * 1000.f)));

						HitActivator->Register([this]()
							{
								LOG_DEBUG("Attack!");
								//Knight Monster 공격
								if(CurrentCombo==1)
									CreateAttackOBB(-1.f, ActType::BaseAttack_1, 80.f, Eigen::Vector3f(75.f, 75.f, 75.f), Eigen::Vector3f(100.f, 0.f, 100.f), false);
								else
									CreateAttackOBB(-1.f, ActType::BaseAttack_1, 80.f, Eigen::Vector3f(100.f, 100.f, 100.f), Eigen::Vector3f(100.f, 0.f, 100.f), false);
							});

						CurrentGameState->AddToTimer(HitActivator);
						CurrentActivators.emplace_back(HitActivator);
					}
					else if (Notify.first.find("AttackEnd") != string::npos)
					{
						shared_ptr<CheckActivator> AttackEndActivator = make_shared<CheckActivator>();
						float hitCheckTime = Notify.second;

						AttackEndActivator->SetTimer(chrono::milliseconds(long long(hitCheckTime * 1000.f)));

						AttackEndActivator->Register([this]()
							{
								CanChase = true;
							});

						CurrentGameState->AddToTimer(AttackEndActivator);
					}
				}
			}
		}


		CurrentGameState->BroadcastPacket(AttackMessage);

	}
}
float KnightMonster::GetHitResetTime(bool IsBigHit)
{
	if (KnightMonsterAnimNotifies)
	{
		if (!IsBigHit)
		{
			auto iter = KnightMonsterAnimNotifies->find("KnightMonster_Hit");

			if (iter != KnightMonsterAnimNotifies->end())
			{
				auto& Notifies = iter->AnimNotifies;

				for (auto& Notify : Notifies)
				{
					if (Notify.first.find("HitReset") != std::string::npos)
						return Notify.second;
				}
			}
		}
		else
			return 2.75f;
	}

	return 0.0f;
}
//void KnightMonster::OnInput(Serializables::Input inputValue)
//{
//	switch (inputValue.InputValue)
//	{
//	//case InputType::Hit_Reset:
//	//{
//	//	IsHit = false;
//	//	break;
//	//}
//	//case InputType::Hit_Check:
//	//{
//	//	LOG_DEBUG("Hit Check!!");
//	//	auto TriggerId = CurrentGameState->SpawnActor(ActorType::Trigger, Eigen::Vector3f(0.f, 0.f, 0.f));
//	//	auto TriggerActor = std::reinterpret_pointer_cast<Trigger>((*CurrentGameState->GetActors())[TriggerId]);
//	//	auto MyLocation = WorldTransform.GetWorldTranslation();
//	//	auto MyRotation = WorldTransform.GetWorldRotation();
//	//	TriggerActor->SetTranslation(MyLocation.x(), MyLocation.y(), MyLocation.z());
//	//	TriggerActor->Rotate(MyRotation);
//
//	//	auto TriggerLocation = TriggerActor->GetTransform().GetWorldTranslation();
//
//	//	 //셋팅 방법
//	//	 //1. Damage 설정
//	//	TriggerActor->SetAsDamager(100.f);
//
//	//	 //2. Volume 추가
//	//	auto NewTriggerAABB = TriggerActor->AddTriggerVolume<AABB>();
//	//	auto NewTriggerOBB = TriggerActor->AddTriggerVolume<OBB>();
//	//	auto NewTriggerSphere = TriggerActor->AddTriggerVolume<BoundingSphere>();
//	//	auto NewTriggerCapsule = TriggerActor->AddTriggerVolume<BoundingCapsule>();
//
//	//	 //3. Volume 위치, 크기 설정
//	//	NewTriggerOBB->SetExtent(Eigen::Vector3f(100.f, 100.f, 100.f));
//	//	NewTriggerOBB->SetRelativeLocation(Eigen::Vector3f(100.f, 0.f, 100.f));
//
//	//	TriggerActor->SetAsAttack(80.f, -1.f, static_cast<TheShift::ActType>(PrevAttackNum), CurrentGameState->GetActor(Id));
//	//	auto NewTriggerOBB = TriggerActor->AddTriggerVolume<OBB>();
//	//	NewTriggerOBB->SetExtent(Eigen::Vector3f(150.f, 150.f, 150.f));
//	//	NewTriggerOBB->SetRelativeLocation(Eigen::Vector3f(100.f, 0.f, 100.f));
//
//	//	auto WorldTranslation = NewTriggerOBB->GetTransform().GetWorldTranslation();
//	//	LOG_DEBUG("OBBTrigger Location: %f, %f, %f", WorldTranslation.x(), WorldTranslation.y(), WorldTranslation.z());
//	//	break;
//	//}
//	}
//}
#endif

}
