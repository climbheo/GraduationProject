﻿#include "Game/Actors/Actor.h"

#ifndef TS_TRIGGER_H
#define TS_TRIGGER_H

namespace TheShift
{
class Trigger : public TheShift::Actor
{
public:
	Trigger(UID Id);

	template<typename VolumeT>
	typename std::shared_ptr<VolumeT> AddTriggerVolume();
	void SetOnTriggerEnterFunc(std::function<void(std::shared_ptr<Actor>)> Func);
	void SetOnTriggerLeaveFunc(std::function<void(std::shared_ptr<Actor>)> Func);
	void SetOnTriggerOverlapFunc(std::function<void(std::shared_ptr<BoundingVolume>)> Func);
	void SetAsDamager(float Damage);
	void SetAsDamager(float Damage, float Timer);
	void SetAsAttack(float Damage, float Timer, ActType AttackType, std::shared_ptr<Actor> Attacker, bool SetDead = false, bool IsKnockBack = true);
	void SetAsSpawner(const char* Tag);
	void SetLifeTime(float Duration);
	void SetTimer(float Duration);

	float GetLifeTime();
	float GetTimerLeftTime();
	void ResetOverlapMark();
	void TriggerLeaveCheck();

private:
	std::vector<std::shared_ptr<BoundingVolume>> TriggerVolumes;
	std::vector<std::pair<std::shared_ptr<Actor>, bool>> OverlappedActors;
	std::function<void(std::shared_ptr<Actor>)> OnTriggerEnterFunc = nullptr;
	std::function<void(std::shared_ptr<Actor>)> OnTriggerLeaveFunc = nullptr;
	std::function<void(std::shared_ptr<BoundingVolume>)> OnTriggerOverlapFunc = nullptr;
	// Trigger의 생존시간 in seconds.
	// -값이면 계속 지속
	float LifeTime = -1.f;
	float TimerTime = -1.f;

	bool SendDebugInfo = false;
};
}
#endif
