﻿#pragma once
#include "Game/Actors/Monster.h"


namespace TheShift
{
class FatMonster : public Monster
{
public:
	FatMonster(UID Id);

#ifdef TSSERVER
	void OnPlayerContact(std::shared_ptr<Actor> Player) override;

	// LookForPlayer()가 INVALID_UID외의 UID를 return할 시 해당 Player를 따라간다.
	virtual void OnRecognizePlayer(std::shared_ptr<Actor> Player)override;

	virtual float GetHitResetTime(bool IsBigHit = false) override;

	virtual void DecidePattern()override;
#endif


private:
#ifdef TSSERVER
	bool AttackCoolDown = true;
	int  PrevAttackNum = 1;

	static const std::set<AnimNotifyInfo>* FatMonsterAnimNotifies;
#endif
};
}
