﻿#include "InventoryComponent.h"
#include "ItemComponent.h"
#include "WeaponItemComponent.h"
#include "ArmorItemComponent.h"
#include "Game/Actors/Actor.h"
#include "Protocol/Log.h"


namespace TheShift
{
InventoryComponent::InventoryComponent()
{
}

InventoryComponent::InventoryComponent(std::shared_ptr<Actor> Owner)
{
	OwnerActor = Owner;
}

void InventoryComponent::SetOwnerActor(std::shared_ptr<Actor> Owner)
{
	OwnerActor = Owner;
}

void InventoryComponent::EquipItem(int ItemIndex)
{
	if(OwnerActor)
	{
		if(Items.find(ItemIndex) != Items.end())
		{
			OwnerActor->EquipItem(Items[ItemIndex]);
		}
	}
}

void InventoryComponent::EquipItem(std::shared_ptr<ItemComponent> Item)
{
	if(OwnerActor)
	{
		for(auto& ItemPair : Items)
		{
			if(ItemPair.second == Item)
			{
				OwnerActor->UnequipItem(Item);
			}
		}
	}
}

std::shared_ptr<ItemComponent> InventoryComponent::UnequipItem(int ItemIndex)
{
	if(OwnerActor)
	{
		if (Items.find(ItemIndex) != Items.end())
			OwnerActor->UnequipItem(Items[ItemIndex]);
		return Items[ItemIndex];
	}

	return nullptr;
}

std::shared_ptr<ItemComponent> InventoryComponent::UnequipItem(std::shared_ptr<ItemComponent> Item)
{
	if(OwnerActor)
	{
		OwnerActor->UnequipItem(Item);
		return Item;
	}

	return nullptr;
}

void InventoryComponent::AcquireItem(std::shared_ptr<ItemComponent> Item)
{
	Item->Index = IndexCount;
	Items.insert_or_assign(IndexCount, Item);
	Items[IndexCount] = Item;

	// Owner 등록
	if (OwnerActor != nullptr)
	{
		Item->SetOwnerActor(OwnerActor);
		Item->SetOwnerInventory(OwnerActor->GetInventoryComp());
	}
	else
		LOG_WARNING("Trying to set owner actor. Can't find owner actor.");

	++IndexCount;
}

std::shared_ptr<ItemComponent> InventoryComponent::UnacquireItem(int ItemIndex)
{
	std::shared_ptr<ItemComponent> ReturnValue = nullptr;

	if(Items.find(ItemIndex) != Items.end())
	{
		ReturnValue = Items[ItemIndex];
		Items.erase(ItemIndex);
	}

	return ReturnValue;
}

std::shared_ptr<ItemComponent> InventoryComponent::UnacquireItem(std::shared_ptr<ItemComponent> Item)
{
	for(auto Iter = Items.begin(); Iter != Items.end();)
	{
		if (Iter->second == Item)
		{
			Items.erase(Iter);
			return Item;
		}
		else
			Iter++;
	}

	return nullptr;
}
}
