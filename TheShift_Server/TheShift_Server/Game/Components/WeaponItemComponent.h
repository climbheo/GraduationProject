﻿#pragma once

#include "ItemComponent.h"


namespace TheShift
{
class WeaponItemComponent : public ItemComponent
{
public:
	WeaponItemComponent();

	int GetItemType() const override;
	
	WeaponItemType WeaponType;

private:
	
};
}
