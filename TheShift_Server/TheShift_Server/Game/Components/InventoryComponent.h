﻿#pragma once

#include "Protocol/Types.h"
#include <map>
#include <memory>

namespace TheShift
{
class ItemComponent;
class Actor;
class InventoryComponent
{
public:
	InventoryComponent();
	InventoryComponent(std::shared_ptr<Actor> Owner);

	void SetOwnerActor(std::shared_ptr<Actor> Owner);
	
	void EquipItem(int Index);
	void EquipItem(std::shared_ptr<ItemComponent> Item);
	std::shared_ptr<ItemComponent> UnequipItem(int ItemIndex);
	std::shared_ptr<ItemComponent> UnequipItem(std::shared_ptr<ItemComponent> Item);
	void AcquireItem(std::shared_ptr<ItemComponent> Item);
	std::shared_ptr<ItemComponent> UnacquireItem(int ItemIndex);
	std::shared_ptr<ItemComponent> UnacquireItem(std::shared_ptr<ItemComponent> Item);


	
	std::map<int, std::shared_ptr<ItemComponent>> Items;

private:
	std::shared_ptr<Actor> OwnerActor = nullptr;
	int IndexCount = 0;
};
}
