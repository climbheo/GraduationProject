﻿#pragma once

#include "ItemComponent.h"


namespace TheShift
{
class ArmorItemComponent : public ItemComponent
{
public:
	ArmorItemComponent();

	int GetItemType() const override;

	ArmorItemType ArmorType;

private:
	
};
}
