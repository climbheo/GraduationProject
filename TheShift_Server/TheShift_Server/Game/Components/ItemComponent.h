﻿#pragma once

#include "Protocol/Types.h"
#include <memory>
#include <string>

namespace TheShift
{
class Actor;
class InventoryComponent;
class ItemComponent
{
public:
	ItemComponent();
	~ItemComponent();

	std::shared_ptr<Actor> GetOwnerActor();
	std::shared_ptr<InventoryComponent> GetOwnerInventoryComponent();
	virtual int GetItemType() const;

	void SetOwnerActor(std::shared_ptr<Actor> NewOwnerActor);
	void SetOwnerInventory(std::shared_ptr<InventoryComponent> NewOwnerInventory);

	int Index;
	std::string Name;
	ItemType ItemCategory;

protected:
	std::shared_ptr<Actor> OwnerActor = nullptr;
	std::shared_ptr<InventoryComponent> OwnerInventoryComponent = nullptr;
};
}
