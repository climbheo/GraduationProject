﻿#pragma once

#include "Game/Components/Transform.h"
#include "Protocol/Types.h"
#include <functional>
#include <set>

namespace TheShift
{

constexpr float StandingOnPlatformDistErrorRange = 2.f;

bool SolveQuadratic(float a, float b, float c, float& x0, float& x1);
Eigen::Vector3f ClosestPointOnLineSegment(Eigen::Vector3f& A, Eigen::Vector3f& B, Eigen::Vector3f& Point);
Eigen::Vector3f GetSlidingVector(const Eigen::Vector3f& p, const Eigen::Vector3f& n);

class Actor;

// OTHER의 Type이 Base일 경우 Type을 알아내고 OTHER.IsOverlapped(*this)로 충돌처리를 진행한다.
// 충돌 여부는 bRET_TYPE으로 return한다.
#define CHECK_VOLUMETYPE_AND_DO_COLLIDE(bRET_TYPE, OTHER)                                             \
	auto OtherParentActor = OTHER.GetParentActor();                                                   \
	auto MyParentActor = GetParentActor();                                                            \
																									  \
	/* 둘중 하나가 Parent가 없거나 같은 Parent를 가지고 있다면 충돌체크 하지 않는다. */               \
	if(OtherParentActor == nullptr || MyParentActor == nullptr || OtherParentActor == MyParentActor)  \
		bRET_TYPE = false;                                                                            \
	else{                                                                                             \
		bool MyIsMovable = MyParentActor->GetIsMovable();                                             \
		bool OtherIsMovable = OtherParentActor->GetIsMovable();                                       \
																									  \
		switch (OTHER.GetVolumeType())                                                                \
		{                                                                                             \
			case BoundingVolumeType::AABB:                                                            \
			{                                                                                         \
				AABB* Temp = static_cast<AABB*>(&OTHER);                                              \
				if (Temp != nullptr)                                                                  \
					bRET_TYPE = Temp->Intersect(*this);                                               \
				break;                                                                                \
			}                                                                                         \
																									  \
			case BoundingVolumeType::OBB:                                                             \
			{                                                                                         \
				OBB* Temp = static_cast<OBB*>(&OTHER);                                                \
				if (Temp != nullptr)                                                                  \
					bRET_TYPE = Temp->Intersect(*this);                                               \
				break;                                                                                \
			}                                                                                         \
																									  \
			case BoundingVolumeType::Sphere:                                                          \
			{                                                                                         \
				BoundingSphere* Temp = static_cast<BoundingSphere*>(&OTHER);                          \
				if (Temp != nullptr)                                                                  \
					bRET_TYPE = Temp->Intersect(*this);                                               \
				break;                                                                                \
			}                                                                                         \
																									  \
			case BoundingVolumeType::Capsule:                                                         \
			{                                                                                         \
				BoundingCapsule* Temp = static_cast<BoundingCapsule*>(&OTHER);                        \
				if (Temp != nullptr)                                                                  \
					bRET_TYPE = Temp->Intersect(*this);                                               \
				break;                                                                                \
			}                                                                                         \
																									  \
			default:                                                                                  \
			{                                                                                         \
				bRET_TYPE = false;                                                                    \
				break;                                                                                \
			}                                                                                         \
		 }                                                                                            \
	}

class AABB;
class OBB;
class BoundingSphere;
class BoundingCapsule;
class CollisionResolver;

class Ray
{
public:
	Ray();

	const Eigen::Vector3f& GetCollisionNormal() const;
	const Eigen::Vector3f& GetCollisionPoint() const;
	float GetLength() const;

	bool Intersect(BoundingVolume* Other, float* ContactDist = nullptr, float RayRadius = 0.f, bool Precise = true);
	bool Intersect(AABB& Other, float* ContactDist = nullptr, float RayRadius = 0.f, bool Precise = true);
	bool Intersect(OBB& Other, float* ContactDist = nullptr, float RayRadius = 0.f, bool Precise = true);
	bool Intersect(BoundingCapsule& Other, float* ContactDist = nullptr, float RayRadius = 0.f);
	bool Intersect(BoundingSphere& Other, float* ContactDist = nullptr, float RayRadius = 0.f);

	bool IntersectSphere(const Eigen::Vector3f& Location, float Radius, float* ContactDist = nullptr);

	void SetEndPoint(const Eigen::Vector3f& End);
	void SetLength(float NewLength);
	bool IsEndless() const;

	Eigen::Vector3f StartPoint;
	Eigen::Vector3f Direction;
private:
	float Length;
	Eigen::Vector3f CollisionNormal;
	Eigen::Vector3f CollisionPoint;
	bool HasEnd = false;
};

class BoundingVolume
{
public:
	BoundingVolume();
	BoundingVolume(Eigen::Vector3f& RelativeLocation, Eigen::Vector3f& RelativeRotation);
	virtual ~BoundingVolume();

	void SetCollisionResolver(CollisionResolver* Owner);

	// 어떤 boundingVolume인지 return한다.
	BoundingVolumeType GetVolumeType() const;
	// 충돌 시 어떻게 행동되어야 할지 그 type을 return한다.
	CollisionType GetCollisionBehavior() const;
	Transform& GetTransform();
	Eigen::Vector3f GetLastTranslation() const;
	Eigen::Vector3f GetLastDegrees() const;
	void SetLastTranslation(const Eigen::Vector3f& LastValue);
	void SetLastTranslation();
	void SetLastDegrees(const Eigen::Vector3f& LastValue);

	void GetLastCollisionPlaneInfo(Eigen::Vector3f& PlaneNormalVector, Eigen::Vector3f& PlaneLocation) const;
	void SetLastCollisionPlaneInfo(Eigen::Vector3f& PlaneNormalVector, Eigen::Vector3f& PlaneLocation);
	float GetLastIntersectedLength() const;
	void SetLastIntersectedLength(float NewLength);
	//const Transform& GetWorldTransform() const;
	//void UpdateWorldTransform();


	std::shared_ptr<Actor> GetParentActor() const;
	std::shared_ptr<BoundingVolume> GetParentVolume() const;

	Eigen::Vector3f GetRelativeLocation() const;
	Eigen::Vector3f GetRelativeRotation() const;
	virtual Eigen::Vector3f GetExtent() const;
	virtual float GetHalfHeight() const;
	virtual float GetRadius() const;
	bool GetIsMovable() const;
	UID GetId() const;

	void SetCollisionBehavior(CollisionType type);
	void SetParent(std::shared_ptr<Actor> Parent);
	void SetParent(std::shared_ptr<BoundingVolume> Parent);
	void SetAsTrigger();
	void SetAsCollider();
	//void SetOnTriggerEnter(std::function<void(std::shared_ptr<BoundingVolume>)> OnTriggerEngerFunc);
	//void SetOnTriggerLeave(std::function<void(std::shared_ptr<BoundingVolume>)> OnTriggerLeaveFunc);
	void SetOnTriggerOverlap(std::function<void(std::shared_ptr<BoundingVolume>)> OnTriggerOverlapFunc);
	void SetRelativeLocation(const Eigen::Vector3f& NewRelativeLocation);
	void SetRelativeRotation(const Eigen::Vector3f& NewRelativeRotation);
	void SetRelativeLocation(const TheShift::Vector3f& NewRelativeLocation);
	void SetRelativeRotation(const TheShift::Vector3f& NewRelativeRotation);

	void SetIsMovable(bool Movable);
	void SetId(UID NewId);

	bool IsTrigger() const;
	bool IsCollider() const;

	void OnTriggerEnter(std::shared_ptr<BoundingVolume> Other);
	void OnTriggerLeave(std::shared_ptr<BoundingVolume> Other);
	void OnTriggerOverlap(std::shared_ptr<BoundingVolume> Other);

	// 충돌처리를 한 후 충돌 여부를 return한다.
	virtual bool Intersect(BoundingVolume& other) = 0;
	virtual bool Intersect(Ray& Other, float* ContactDist = nullptr, float RayRadius = 0.f) = 0;
	virtual void Resolve(float deltaTime) = 0;

	// 회전된 Extent를 축에 재정렬 시켜 return한다.

	Eigen::Vector3f GetMaxPoint();
	Eigen::Vector3f GetMinPoint();
	virtual void CalculateAlignedExtent();
	Eigen::Vector3f GetAlignedExtent();
	void NotifyExtentUpdateNeeded();

	void RegisterBoundingVolumeRearranger(std::function<void(BoundingVolume*)> Rearranger);

	float GetCollisionRange() const;

	std::set<UID> CloseVolumes;

protected:
	// 충돌 시 어떻게 처리되어야 할지 나타낸다.
	CollisionType CollisionBehavior;
	// 어떤 타입의 boundingVolume인지 나타낸다.
	BoundingVolumeType VolumeType;
	Transform LocalTransform;
	Eigen::Vector3f LastTranslation;
	Eigen::Vector3f LastDegrees;


	// Extent, Parent등 모든게 초기화가 완료 되어있는지 나타냄.
	bool IsInitialized = false;

	bool IsTrigger_ = false;
	bool IsCollider_ = false;
	bool IsMovable = true;

	// Rotate를 하여서 Extent를 다시 계산해야 할때 true로 변경한다.
	// Extent를 Get할때마다 이 값을 체크, true면 다시 계산하고 false로 바꾼다.
	// false면 계산하지 않고 그냥 return한다.
	bool ExtentUpdateNeeded = false;

	//std::function<void(std::shared_ptr<BoundingVolume>)> OnTriggerEnterFunc = nullptr;
	//std::function<void(std::shared_ptr<BoundingVolume>)> OnTriggerLeaveFunc = nullptr;
	std::function<void(std::shared_ptr<BoundingVolume>)> OnTriggerOverlapFunc = nullptr;
	//void ApplyWorldlTransform(Transform& worldTransform);

	// 충돌 법선벡터
	Eigen::Vector3f CollisionPlaneNormalVector;
	Eigen::Vector3f CollisionPlaneLocation;
	float IntersectedLength;

	// 회전이 적용된 AABB Extent
	Eigen::Vector3f CalculatedAlignedExtent;

	float CollisionRange = 0.f;

	CollisionResolver* Resolver;

private:
	// 둘중 하나만 set 되어야 함.
	std::shared_ptr<Actor> ParentActor = nullptr;
	std::shared_ptr<BoundingVolume> ParentVolume = nullptr;
	UID Id;
};

class AABB : public BoundingVolume
{
public:
	AABB();
	AABB(Eigen::Vector3f& RelativeLocation,
		 Eigen::Vector3f& RelativeRotation,
		 Eigen::Vector3f& NewExtent);

	bool Intersect(BoundingVolume& other) override;
	bool Intersect(AABB& other);
	bool Intersect(OBB& other);
	bool Intersect(BoundingSphere& other);
	bool Intersect(BoundingCapsule& other);
	bool Intersect(Eigen::Vector3f& Ray, Eigen::Vector3f& StartPoint, Eigen::Vector3f& EndPoint, float& Dist, float Radius = 0.f);

	bool Intersect(Ray& Other, float* ContactDist = nullptr, float RayRadius = 0.f) override;

	void Resolve(float deltaTime) override;

	bool Contains(Eigen::Vector3f& Point);

	static Eigen::Vector3f CalculateExtent(Eigen::Vector3f& Extent, const Eigen::Quaternionf& WorldRotation);

	Eigen::Vector3f GetExtent() const override;
	void SetExtent(const Eigen::Vector3f& NewExtent);
	void SetExtent(const TheShift::Vector3f& NewExtent);
	void SetAlignedExtent(const Eigen::Vector3f& NewExtent);
	void SetAlignedExtent(const TheShift::Vector3f& NewExtent);

private:
	Eigen::Quaternionf Rotation;
	Eigen::Quaternionf RotationOffset;
	Eigen::Quaternionf LastRotation;

	Eigen::Vector3f Extent;

	virtual void CalculateAlignedExtent() override;
};

class OBB : public BoundingVolume
{
public:
	OBB();
	OBB(Eigen::Vector3f& RelativeLocation,
		Eigen::Vector3f& RelativeRotation,
		Eigen::Vector3f& NewExtent);
	//OBB(BoundingVolumeVertexInfo& midAndExtent);

	bool Intersect(BoundingVolume& other) override;
	bool Intersect(AABB& other);
	bool Intersect(OBB& other);
	bool Intersect(BoundingSphere& other);
	bool Intersect(BoundingCapsule& other);

	bool Intersect(Ray& Other, float* ContactDist = nullptr, float RayRadius = 0.f) override;

	void Resolve(float deltaTime) override;

	Eigen::Vector3f GetExtent() const override;
	void SetExtent(const Eigen::Vector3f& NewExtent);
	void SetExtent(const TheShift::Vector3f& NewExtent);

	bool Separated(OBB& Other, const Eigen::Vector3f& Axis);
	void GetCorners(Eigen::Vector3f* Corners);
	Eigen::Vector3f GetCorner(const Eigen::Vector3f& Dir);


private:
	Eigen::Vector3f Extent;

	virtual void CalculateAlignedExtent() override;
};

class BoundingCapsule : public BoundingVolume
{
public:
	BoundingCapsule();
	BoundingCapsule(Eigen::Vector3f& RelativeLocation,
					Eigen::Vector3f& RelativeRotation,
					float NewHalfHeight,
					float NewRadius);

	std::vector<std::shared_ptr<BoundingSphere>> Spheres;

	bool Intersect(BoundingVolume& other) override;
	bool Intersect(AABB& other);
	bool Intersect(OBB& other);
	bool Intersect(BoundingSphere& other);
	bool Intersect(BoundingCapsule& other);

	bool Intersect(Ray& Other, float* ContactDist = nullptr, float RayRadius = 0.f) override;

	void Resolve(float deltaTime) override;

	//void Resolve(float deltaTime) override;
	//void Resolve(OBB& other, float deltaTime);
	//void Resolve(BoundingCapsule& other, float deltaTime);

	float GetRadius() const override;
	float GetHalfHeight() const override;

	void SetRadius(float NewRadius);
	void SetHalfHeight(float NewHalfHeight);

	void CreateDerivedSpheres();

private:
	Eigen::Quaternionf Rotation;
	Eigen::Quaternionf RotationOffset;
	Eigen::Quaternionf LastRotation;
	float Radius;
	float HalfHeight;

	virtual void CalculateAlignedExtent() override;
};

class BoundingSphere : public BoundingVolume
{
public:
	BoundingSphere();
	BoundingSphere(Eigen::Vector3f& RelativeLocation,
				   Eigen::Vector3f& RelativeRotation,
				   float NewRadius);
	//BoundingSphere(BoundingVolumeVertexInfo& midAndExtent);

	bool Intersect(BoundingVolume& other) override;
	bool Intersect(AABB& other);
	bool Intersect(OBB& other);
	bool Intersect(BoundingSphere& other);
	bool Intersect(BoundingCapsule& other);

	bool Intersect(Ray& Other, float* ContactDist = nullptr, float RayRadius = 0.f) override;

	void Resolve(float deltaTime) override;

	float GetRadius() const override;

	void SetRadius(float NewRadius);

	bool IsPartOfCapsule() const;
	bool IsTopSphereOfCapsule() const;
	bool IsBottomSphereOfCapsule() const;
	void SetAsDerivedSphere();
	void SetAsTopSphereOfCapsule();
	void SetAsBottomSphereOfCapsule();

private:
	float Radius;
	bool IsDerived = false;
	bool IsBottomOfCapsule = false;
	bool IsTopOfCapsule = false;
	virtual void CalculateAlignedExtent() override;
};
}

